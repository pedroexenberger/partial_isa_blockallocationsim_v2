# Partial_ISA_BlockAllocationSim_v2

A simulator for a heterogeneous system partial-ISA capable. Cores of the system can have partial or full support for ISA instructions. 
Partial-ISA enables area and power reductions, at the cost of lack of support for some types of instructions. Area and power gains can
be reverted into more cores (simple ones) or hardware accelerators. 
The present tool simulate the mapping of workloads with a execution unit (processor, hardware accelerators) in the system that is capable to handle it, based on the trace of blocks of a given instruction type/extension.
It considers ISA support, task migration costs, execution unit power costs, and so on. 

## Dependencies

- `pip3 install ptvsd` to debug with vs code (can be commented out if you don't want to debug)
- `pip3 install plotly` to generate charts
- `pip3 install pandas` to manage data for generating charts
- `pip3 install psutil` to save charts in vectorized images
- `npm install -g electron@1.8.4 orca`
- `pip3 install scipy`to statistics and other math utils
- other packages are standard
- developed/tested with python 3.6.7 64-bit version

## Run tests

`cd test`

To test all methods from a module
`python3 -m unittest TestBlock -v`

To test a specific method
`python3 -m  unittest TestWorkload.TestWorkload.test_read_block`

## Run simulation
`cd src`
`./Simulate.py ../configurations/big_little.json ../scenarios/test_scenario.json`
