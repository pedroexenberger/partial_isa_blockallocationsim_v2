######################################################################

# Created by Genus(TM) Synthesis Solution GENUS15.22 - 15.20-s024_1 on Thu Mar 28 23:43:43 -0300 2019

# This file contains the RC script for /designs/nova

######################################################################

::legacy::set_attribute -quiet init_lib_search_path /home/gme/pedro.becker/NanGate_15nm_OCL_v0.1_2014_06.A/front_end/timing_power_noise/CCS/ /
::legacy::set_attribute -quiet common_ui false /
::legacy::set_attribute -quiet design_mode_process no_value /
::legacy::set_attribute -quiet phys_assume_met_fill 0.0 /
::legacy::set_attribute -quiet runtime_by_stage { {to_generic 128 195 89 153}  {first_condense 273 560 135 393}  {reify 210 770 73 467}  {global_incr_map 103 914 75 582}  {IOPT_RUNTIME_INIT 0 989 0 638}  {IOPT_RUNTIME_INIT_TIM 7 997 7 645}  {IOPT_RUNTIME_SEQ_MBCI 0 998 0 646}  {IOPT_RUNTIME_CRB_FAST_CLEANUP 0 998 0 647}  {IOPT_RUNTIME_CRB_WNS 0 998 0 647}  {IOPT_RUNTIME_CRB_WNS_CRR 1 999 0 647}  {IOPT_RUNTIME_START 7 1008 7 657}  {IOPT_RUNTIME_LATCH_OPTO 0 1008 0 657}  {IOPT_RUNTIME_AREA_POWER 83 1091 76 733}  {IOPT_RUNTIME_WNS 0 1091 0 733}  {IOPT_RUNTIME_TNS 0 1091 0 733}  {IOPT_RUNTIME_DRC 0 1091 0 733}  {IOPT_RUNTIME_AREA_POWER 28 1119 27 761} } /
::legacy::set_attribute -quiet tinfo_tstamp_file .rs_pedro.becker.tstamp /
::legacy::set_attribute -quiet wireload_mode top /
::legacy::set_attribute -quiet lp_power_analysis_effort high /
::legacy::set_attribute -quiet lp_default_toggle_rate 0.10000 /
::legacy::set_attribute -quiet super_thread_servers {localhost localhost localhost localhost localhost localhost localhost localhost   } /
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2161/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2163/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2164/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2165/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2167/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2168/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2169/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2172/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2173/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2174/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2175/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2176/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2177/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2178/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2179/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2180/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2181/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2182/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g1550/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2821/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2822/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2823/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2824/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2825/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2826/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2827/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2828/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2829/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2830/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2831/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2832/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2833/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2834/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2835/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2836/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2837/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2838/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2839/pins_in/A1
::legacy::set_attribute -quiet break_timing_paths true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2840/pins_in/A1
::legacy::set_attribute -quiet phys_use_segment_parasitics true /
::legacy::set_attribute -quiet probabilistic_extraction true /
::legacy::set_attribute -quiet ple_correlation_factors {1.9000 2.0000} /
::legacy::set_attribute -quiet maximum_interval_of_vias infinity /
::legacy::set_attribute -quiet interconnect_mode wireload /
::legacy::set_attribute -quiet wireload_mode top /
::legacy::set_attribute -quiet tree_type balanced_tree /libraries/NanGate_15nm_OCL/operating_conditions/typical
::legacy::set_attribute -quiet tree_type balanced_tree /libraries/NanGate_15nm_OCL/operating_conditions/_nominal_
# BEGIN MSV SECTION
# END MSV SECTION
define_clock -name clk -domain domain_1 -period 5000.0 -divide_period 1 -rise 0 -divide_rise 1 -fall 1 -divide_fall 2 -design /designs/nova /designs/nova/ports_in/clk
::legacy::set_attribute -quiet waveform { } /designs/nova/timing/clock_domains/domain_1/clk
define_cost_group -design /designs/nova -name clk
external_delay -accumulate -input {0.0 no_value 0.0 no_value} -clock /designs/nova/timing/clock_domains/domain_1/clk -name create_clock_delay_domain_1_clk_R_0 /designs/nova/ports_in/clk
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/create_clock_delay_domain_1_clk_R_0
external_delay -accumulate -input {no_value 0.0 no_value 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -edge_fall -name create_clock_delay_domain_1_clk_F_0 /designs/nova/ports_in/clk
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/create_clock_delay_domain_1_clk_F_0
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_1 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2181/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_1
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_1
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_2 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2161/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_2
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_2
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_3 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2174/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_3
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_3
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_4 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2164/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_4
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_4
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_5 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2178/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_5
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_5
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_6 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2172/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_6
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_6
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_7 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2167/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_7
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_7
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_8 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2177/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_8
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_8
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_9 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2176/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_9
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_9
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_10 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2180/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_10
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_10
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_11 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2182/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_11
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_11
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_12 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2165/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_12
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_12
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_13 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2168/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_13
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_13
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_14 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2169/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_14
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_14
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_15 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2163/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_15
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_15
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_16 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2173/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_16
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_16
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_17 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2175/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_17
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_17
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_18 /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_comb/g2179/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_18
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_18
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_19 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2832/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_19
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_19
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_20 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2835/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_20
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_20
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_21 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2825/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_21
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_21
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_22 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2826/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_22
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_22
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_23 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2827/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_23
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_23
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_24 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2828/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_24
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_24
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_25 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2829/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_25
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_25
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_26 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2830/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_26
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_26
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_27 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2831/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_27
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_27
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_28 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g1550/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_28
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_28
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_29 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2833/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_29
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_29
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_30 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2834/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_30
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_30
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_31 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2836/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_31
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_31
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_32 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2837/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_32
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_32
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_33 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2838/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_33
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_33
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_34 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2839/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_34
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_34
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_35 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2840/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_35
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_35
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_36 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2821/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_36
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_36
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_37 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2822/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_37
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_37
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_38 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2823/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_38
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_38
external_delay -accumulate -output {no_value no_value 0.0 0.0} -clock /designs/nova/timing/clock_domains/domain_1/clk -name clk_gating_check_39 /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/instances_comb/g2824/pins_in/A1
::legacy::set_attribute -quiet clock_network_latency_included true /designs/nova/timing/external_delays/clk_gating_check_39
::legacy::set_attribute -quiet clock_source_latency_included true /designs/nova/timing/external_delays/clk_gating_check_39
path_group -paths [specify_paths -to /designs/nova/timing/clock_domains/domain_1/clk]  -name clk -group /designs/nova/timing/cost_groups/clk -user_priority -1047552
# BEGIN DFT SECTION
::legacy::set_attribute -quiet dft_scan_style muxed_scan /
::legacy::set_attribute -quiet dft_scanbit_waveform_analysis false /
# END DFT SECTION
::legacy::set_attribute -quiet qos_by_stage {{to_generic {wns -11111111} {tns -111111111} {vep -111111111} {area 99277} {cell_count 127144} {utilization  0.00} {runtime 128 195 89 153} }{first_condense {wns -11111111} {tns -111111111} {vep -111111111} {area 103924} {cell_count 187305} {utilization  0.00} {runtime 273 560 135 393} }{reify {wns 2011} {tns 0} {vep 0} {area 71811} {cell_count 148372} {utilization  0.00} {runtime 210 777 73 475} }{global_incr_map {wns 1980} {tns 0} {vep 0} {area 71414} {cell_count 147652} {utilization  0.00} {runtime 103 922 75 590} }} /designs/nova
::legacy::set_attribute -quiet qos_data_collection_time 14 /designs/nova
::legacy::set_attribute -quiet hdl_user_name nova /designs/nova
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/BitStream_buffer.v src/BitStream_controller.v src/bitstream_gclk_gen.v src/BitStream_parser_FSM_gating.v src/bs_decoding.v src/cavlc_consumed_bits_decoding.v src/cavlc_decoder.v src/CodedBlockPattern_decoding.v src/dependent_variable_decoding.v src/DF_mem_ctrl.v src/DF_pipeline.v src/DF_reg_ctrl.v src/DF_top.v src/end_of_blk_decoding.v src/exp_golomb_decoding.v src/ext_RAM_ctrl.v src/heading_one_detector.v src/hybrid_pipeline_ctrl.v src/Inter_mv_decoding.v src/Inter_pred_CPE.v src/Inter_pred_LPE.v src/Inter_pred_pipeline.v src/Inter_pred_reg_ctrl.v src/Inter_pred_sliding_window.v src/Inter_pred_top.v src/Intra4x4_PredMode_decoding.v src/Intra_pred_PE.v src/Intra_pred_pipeline.v src/Intra_pred_reg_ctrl.v src/Intra_pred_top.v src/IQIT.v src/level_decoding.v src/nC_decoding.v src/nova.v src/NumCoeffTrailingOnes_decoding.v src/pc_decoding.v src/QP_decoding.v src/ram_async_1r_sync_1w.v src/ram_sync_1r_sync_1w.v src/rec_DF_RAM_ctrl.v src/rec_gclk_gen.v src/reconstruction.v src/run_decoding.v src/sum.v src/syntax_decoding.v src/total_zeros_decoding.v} {./src}}} /designs/nova
::legacy::set_attribute -quiet seq_reason_deleted {{BitStream_controller/syntax_decoding/forbidden_zero_bit unloaded} {BitStream_controller/syntax_decoding/nal_ref_idc unloaded} {BitStream_controller/syntax_decoding/profile_idc unloaded} {BitStream_controller/syntax_decoding/constraint_set0_flag unloaded} {BitStream_controller/syntax_decoding/constraint_set1_flag unloaded} {BitStream_controller/syntax_decoding/constraint_set2_flag unloaded} {BitStream_controller/syntax_decoding/constraint_set3_flag unloaded} {BitStream_controller/syntax_decoding/reserved_zero_4bits unloaded} {BitStream_controller/syntax_decoding/level_idc unloaded} {BitStream_controller/syntax_decoding/seq_parameter_set_id_sps unloaded} {BitStream_controller/syntax_decoding/pic_order_cnt_type unloaded} {BitStream_controller/syntax_decoding/num_ref_frames unloaded} {BitStream_controller/syntax_decoding/gaps_in_frame_num_value_allowed_flag unloaded} {BitStream_controller/syntax_decoding/pic_width_in_mbs_minus1 unloaded} {BitStream_controller/syntax_decoding/pic_height_in_map_units_minus1 unloaded} {BitStream_controller/syntax_decoding/frame_mbs_only_flag unloaded} {BitStream_controller/syntax_decoding/direct_8x8_inference_flag unloaded} {BitStream_controller/syntax_decoding/frame_cropping_flag unloaded} {BitStream_controller/syntax_decoding/vui_parameter_present_flag unloaded} {BitStream_controller/syntax_decoding/pic_parameter_set_id_pps unloaded} {BitStream_controller/syntax_decoding/seq_parameter_set_id_pps unloaded} {BitStream_controller/syntax_decoding/entropy_coding_mode_flag unloaded} {BitStream_controller/syntax_decoding/pic_order_present_flag unloaded} {BitStream_controller/syntax_decoding/num_slice_groups_minus1 unloaded} {BitStream_controller/syntax_decoding/num_ref_idx_l1_active_minus1 unloaded} {BitStream_controller/syntax_decoding/weighted_pred_flag unloaded} {BitStream_controller/syntax_decoding/weighted_bipred_idc unloaded} {BitStream_controller/syntax_decoding/pic_init_qs_minus26 unloaded} {BitStream_controller/syntax_decoding/redundant_pic_cnt_present_flag unloaded} {BitStream_controller/syntax_decoding/first_mb_in_slice unloaded} {BitStream_controller/syntax_decoding/pic_parameter_set_id_slice_header unloaded} {BitStream_controller/syntax_decoding/frame_num unloaded} {BitStream_controller/syntax_decoding/idr_pic_id unloaded} {BitStream_controller/syntax_decoding/pic_order_cnt_lsb unloaded} {BitStream_controller/cavlc_decoder/run_decoding/run_15 unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/LumaLevel_CurrMb3_reg[15]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/LumaLevel_CurrMb3_reg[16]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/LumaLevel_CurrMb3_reg[17]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/LumaLevel_CurrMb3_reg[18]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/LumaLevel_CurrMb3_reg[19]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cb_CurrMb_reg[15]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cb_CurrMb_reg[16]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cb_CurrMb_reg[17]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cb_CurrMb_reg[18]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cb_CurrMb_reg[19]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cr_CurrMb_reg[15]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cr_CurrMb_reg[16]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cr_CurrMb_reg[17]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cr_CurrMb_reg[18]} unloaded} {{BitStream_controller/cavlc_decoder/nC_decoding/ChromaLevel_Cr_CurrMb_reg[19]} unloaded} {{reconstruction/Inter_pred_top/Inter_pred_reg_ctrl/xInt_addr_unclip_reg_reg[0]} unloaded} {{reconstruction/Inter_pred_top/Inter_pred_reg_ctrl/xInt_addr_unclip_reg_reg[1]} unloaded} {{reconstruction/DF_top/DF_pipeline/alpha_PRE_reg[0]} unloaded} {{reconstruction/DF_top/DF_pipeline/alpha_PRE_reg[1]} unloaded} {{BitStream_controller/BitStream_parser_FSM/ref_pic_list_reordering_state_reg[2]} {{merged with BitStream_controller/BitStream_parser_FSM/ref_pic_list_reordering_state_reg[1]}}} {{BitStream_controller/bs_decoding/bs_V1_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_V1_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V1_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_V1_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V1_reg[11]} {{merged with BitStream_controller/bs_decoding/bs_V1_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H1_reg[2]} {{merged with BitStream_controller/bs_decoding/bs_V1_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H1_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_V1_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H1_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_V1_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H1_reg[11]} {{merged with BitStream_controller/bs_decoding/bs_V1_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V2_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_V2_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V2_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_V2_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V2_reg[11]} {{merged with BitStream_controller/bs_decoding/bs_V2_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H2_reg[2]} {{merged with BitStream_controller/bs_decoding/bs_V2_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H2_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_V2_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H2_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_V2_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H2_reg[11]} {{merged with BitStream_controller/bs_decoding/bs_V2_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V3_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_V3_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V3_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_V3_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V3_reg[11]} {{merged with BitStream_controller/bs_decoding/bs_V3_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H3_reg[2]} {{merged with BitStream_controller/bs_decoding/bs_V3_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H3_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_V3_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H3_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_V3_reg[2]}}} {{BitStream_controller/bs_decoding/bs_H3_reg[11]} {{merged with BitStream_controller/bs_decoding/bs_V3_reg[2]}}} {{BitStream_controller/bs_decoding/bs_V3_reg[2]} {{constant 0}}} {{BitStream_controller/bs_decoding/bs_V2_reg[2]} {{constant 0}}} {{BitStream_controller/bs_decoding/bs_V1_reg[2]} {{constant 0}}} {{reconstruction/Intra_pred_top/Intra_pred_pipeline/plane_a_reg_reg[3]} {{constant 0}}} {{reconstruction/Intra_pred_top/Intra_pred_pipeline/plane_a_reg_reg[2]} {{constant 0}}} {{reconstruction/Intra_pred_top/Intra_pred_pipeline/plane_a_reg_reg[1]} {{constant 0}}} {{reconstruction/Intra_pred_top/Intra_pred_pipeline/plane_a_reg_reg[13]} {{constant 0}}} {{reconstruction/Intra_pred_top/Intra_pred_pipeline/plane_a_reg_reg[0]} {{constant 0}}} {{BitStream_controller/BitStream_parser_FSM/ref_pic_list_reordering_state_reg[1]} {{constant 0}}} {{reconstruction/DF_top/DF_pipeline/beta_PRE_reg[5]} {{constant 0}}} {{reconstruction/DF_top/DF_pipeline/beta_PRE_reg[6]} {{constant 0}}} {{reconstruction/DF_top/DF_pipeline/beta_PRE_reg[7]} {{constant 0}}} {{BitStream_controller/BitStream_buffer/buffer_index_reg[1]} {{constant 0}}} {{BitStream_controller/BitStream_buffer/buffer_index_reg[0]} {{constant 0}}} {{BitStream_controller/BitStream_buffer/buffer_index_reg[2]} {{constant 0}}} {{BitStream_controller/BitStream_buffer/buffer_index_reg[3]} {{constant 0}}} {{BitStream_controller/cavlc_decoder/level_decoding/suffixLength_reg[3]} {{constant 0}}} {{reconstruction/DF_top/DF_mem_ctrl/dis_frame_RAM_wr_addr_LeftTop_reg_reg[0]} {{constant 0}}} {{reconstruction/Inter_pred_top/Inter_pred_reg_ctrl/xInt_addr_unclip_reg_reg[2]} unloaded} {{BitStream_controller/bs_decoding/bs_H0_reg[2]} {{merged with BitStream_controller/bs_decoding/bs_H0_reg[11]}}} {{BitStream_controller/bs_decoding/bs_H0_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_H0_reg[11]}}} {{BitStream_controller/bs_decoding/bs_H0_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_H0_reg[11]}}} {{BitStream_controller/bs_decoding/bs_V0_reg[2]} {{merged with BitStream_controller/bs_decoding/bs_V0_reg[11]}}} {{BitStream_controller/bs_decoding/bs_V0_reg[5]} {{merged with BitStream_controller/bs_decoding/bs_V0_reg[11]}}} {{BitStream_controller/bs_decoding/bs_V0_reg[8]} {{merged with BitStream_controller/bs_decoding/bs_V0_reg[11]}}} {{BitStream_controller/cavlc_decoder/total_zeros_decoding/maxNumCoeff_reg[1]} {{merged with BitStream_controller/cavlc_decoder/total_zeros_decoding/maxNumCoeff_reg[0]}}} {{BitStream_controller/cavlc_decoder/total_zeros_decoding/maxNumCoeff_reg[3]} {{merged with BitStream_controller/cavlc_decoder/total_zeros_decoding/maxNumCoeff_reg[0]}}}} /designs/nova
::legacy::set_attribute -quiet verification_directory fv/nova /designs/nova
::legacy::set_attribute -quiet max_dynamic_power 0.0 /designs/nova
::legacy::set_attribute -quiet arch_filename ./src/nova.v /designs/nova
::legacy::set_attribute -quiet entity_filename ./src/nova.v /designs/nova
::legacy::set_attribute -quiet is_ideal_network true /designs/nova/ports_in/clk
::legacy::set_attribute -quiet ideal_network true /designs/nova/ports_in/clk
::legacy::set_attribute -quiet preserve true /designs/nova/nets/clk
::legacy::set_attribute -quiet hdl_user_name BitStream_controller /designs/nova/subdesigns/BitStream_controller
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/BitStream_buffer.v src/BitStream_controller.v src/bitstream_gclk_gen.v src/BitStream_parser_FSM_gating.v src/bs_decoding.v src/cavlc_consumed_bits_decoding.v src/cavlc_decoder.v src/CodedBlockPattern_decoding.v src/dependent_variable_decoding.v src/end_of_blk_decoding.v src/exp_golomb_decoding.v src/heading_one_detector.v src/Inter_mv_decoding.v src/Intra4x4_PredMode_decoding.v src/level_decoding.v src/nC_decoding.v src/NumCoeffTrailingOnes_decoding.v src/pc_decoding.v src/QP_decoding.v src/ram_async_1r_sync_1w.v src/run_decoding.v src/syntax_decoding.v src/total_zeros_decoding.v} {./src}}} /designs/nova/subdesigns/BitStream_controller
::legacy::set_attribute -quiet arch_filename ./src/BitStream_controller.v /designs/nova/subdesigns/BitStream_controller
::legacy::set_attribute -quiet entity_filename ./src/BitStream_controller.v /designs/nova/subdesigns/BitStream_controller
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/nets/clk
::legacy::set_attribute -quiet hdl_user_name BitStream_buffer /designs/nova/subdesigns/BitStream_buffer
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/BitStream_buffer.v} {./src}}} /designs/nova/subdesigns/BitStream_buffer
::legacy::set_attribute -quiet arch_filename ./src/BitStream_buffer.v /designs/nova/subdesigns/BitStream_buffer
::legacy::set_attribute -quiet entity_filename ./src/BitStream_buffer.v /designs/nova/subdesigns/BitStream_buffer
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/BitStream_buffer/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/increment_unsigned
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 17 0 2 1 1 0} /designs/nova/instances_hier/BitStream_controller/instances_hier/BitStream_buffer/instances_hier/inc_add_115_45_1
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/BitStream_parser_FSM
::legacy::set_attribute -quiet hdl_user_name BitStream_parser_FSM /designs/nova/subdesigns/BitStream_parser_FSM
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/BitStream_parser_FSM_gating.v} {./src}}} /designs/nova/subdesigns/BitStream_parser_FSM
::legacy::set_attribute -quiet arch_filename ./src/BitStream_parser_FSM_gating.v /designs/nova/subdesigns/BitStream_parser_FSM
::legacy::set_attribute -quiet entity_filename ./src/BitStream_parser_FSM_gating.v /designs/nova/subdesigns/BitStream_parser_FSM
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/BitStream_parser_FSM/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_11994
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_11999
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_11999_1
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_12009
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/CodedBlockPattern_decoding
::legacy::set_attribute -quiet hdl_user_name CodedBlockPattern_decoding /designs/nova/subdesigns/CodedBlockPattern_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/CodedBlockPattern_decoding.v} {./src}}} /designs/nova/subdesigns/CodedBlockPattern_decoding
::legacy::set_attribute -quiet arch_filename ./src/CodedBlockPattern_decoding.v /designs/nova/subdesigns/CodedBlockPattern_decoding
::legacy::set_attribute -quiet entity_filename ./src/CodedBlockPattern_decoding.v /designs/nova/subdesigns/CodedBlockPattern_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/CodedBlockPattern_decoding/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/Inter_mv_decoding
::legacy::set_attribute -quiet hdl_user_name Inter_mv_decoding /designs/nova/subdesigns/Inter_mv_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_mv_decoding.v} {./src}}} /designs/nova/subdesigns/Inter_mv_decoding
::legacy::set_attribute -quiet arch_filename ./src/Inter_mv_decoding.v /designs/nova/subdesigns/Inter_mv_decoding
::legacy::set_attribute -quiet entity_filename ./src/Inter_mv_decoding.v /designs/nova/subdesigns/Inter_mv_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/Inter_mv_decoding/nets/clk
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width16_data_depth11
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width16_data_depth11
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width16_data_depth11
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width16_data_depth11
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/Intra4x4_PredMode_decoding
::legacy::set_attribute -quiet hdl_user_name Intra4x4_PredMode_decoding /designs/nova/subdesigns/Intra4x4_PredMode_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra4x4_PredMode_decoding.v} {./src}}} /designs/nova/subdesigns/Intra4x4_PredMode_decoding
::legacy::set_attribute -quiet arch_filename ./src/Intra4x4_PredMode_decoding.v /designs/nova/subdesigns/Intra4x4_PredMode_decoding
::legacy::set_attribute -quiet entity_filename ./src/Intra4x4_PredMode_decoding.v /designs/nova/subdesigns/Intra4x4_PredMode_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/Intra4x4_PredMode_decoding/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/QP_decoding
::legacy::set_attribute -quiet hdl_user_name QP_decoding /designs/nova/subdesigns/QP_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/QP_decoding.v} {./src}}} /designs/nova/subdesigns/QP_decoding
::legacy::set_attribute -quiet arch_filename ./src/QP_decoding.v /designs/nova/subdesigns/QP_decoding
::legacy::set_attribute -quiet entity_filename ./src/QP_decoding.v /designs/nova/subdesigns/QP_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/QP_decoding/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/bitstream_gclk_gen
::legacy::set_attribute -quiet hdl_user_name bitstream_gclk_gen /designs/nova/subdesigns/bitstream_gclk_gen
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bitstream_gclk_gen.v} {./src}}} /designs/nova/subdesigns/bitstream_gclk_gen
::legacy::set_attribute -quiet arch_filename ./src/bitstream_gclk_gen.v /designs/nova/subdesigns/bitstream_gclk_gen
::legacy::set_attribute -quiet entity_filename ./src/bitstream_gclk_gen.v /designs/nova/subdesigns/bitstream_gclk_gen
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/increment_unsigned_24086
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 17 0 2 1 1 0} /designs/nova/instances_hier/BitStream_controller/instances_hier/bitstream_gclk_gen/instances_hier/inc_add_116_48_52
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/bs_decoding
::legacy::set_attribute -quiet hdl_user_name bs_decoding /designs/nova/subdesigns/bs_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/bs_decoding
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/bs_decoding
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/bs_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/bs_decoding/nets/clk
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8478
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8478
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8478
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8478
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8477
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8477
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8477
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8477
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8479
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8479
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8479
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8479
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8476
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8476
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8476
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8476
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8486
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8486
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8486
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8486
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8485
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8485
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8485
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8485
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8484
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8484
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8484
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8484
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8475
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8475
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8475
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8475
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8474
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8474
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8474
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8474
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8473
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8473
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8473
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8473
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8472
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8472
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8472
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8472
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8483
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8483
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8483
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8483
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8482
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8482
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8482
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8482
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8481
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8481
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8481
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8481
::legacy::set_attribute -quiet hdl_user_name mv_diff_GE4 /designs/nova/subdesigns/mv_diff_GE4_8480
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/bs_decoding.v} {./src}}} /designs/nova/subdesigns/mv_diff_GE4_8480
::legacy::set_attribute -quiet arch_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8480
::legacy::set_attribute -quiet entity_filename ./src/bs_decoding.v /designs/nova/subdesigns/mv_diff_GE4_8480
::legacy::set_attribute -quiet hdl_user_name cavlc_decoder /designs/nova/subdesigns/cavlc_decoder
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/cavlc_consumed_bits_decoding.v src/cavlc_decoder.v src/end_of_blk_decoding.v src/level_decoding.v src/nC_decoding.v src/NumCoeffTrailingOnes_decoding.v src/ram_async_1r_sync_1w.v src/run_decoding.v src/total_zeros_decoding.v} {./src}}} /designs/nova/subdesigns/cavlc_decoder
::legacy::set_attribute -quiet arch_filename ./src/cavlc_decoder.v /designs/nova/subdesigns/cavlc_decoder
::legacy::set_attribute -quiet entity_filename ./src/cavlc_decoder.v /designs/nova/subdesigns/cavlc_decoder
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/cavlc_decoder/nets/clk
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11_8530
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11_8530
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11_8530
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width10_data_depth11_8530
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width20_data_depth11
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width20_data_depth11
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width20_data_depth11
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width20_data_depth11
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/NumCoeffTrailingOnes_decoding
::legacy::set_attribute -quiet hdl_user_name NumCoeffTrailingOnes_decoding /designs/nova/subdesigns/NumCoeffTrailingOnes_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/NumCoeffTrailingOnes_decoding.v} {./src}}} /designs/nova/subdesigns/NumCoeffTrailingOnes_decoding
::legacy::set_attribute -quiet arch_filename ./src/NumCoeffTrailingOnes_decoding.v /designs/nova/subdesigns/NumCoeffTrailingOnes_decoding
::legacy::set_attribute -quiet entity_filename ./src/NumCoeffTrailingOnes_decoding.v /designs/nova/subdesigns/NumCoeffTrailingOnes_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/cavlc_decoder/instances_hier/NumCoeffTrailingOnes_decoding/nets/clk
::legacy::set_attribute -quiet hdl_user_name cavlc_consumed_bits_decoding /designs/nova/subdesigns/cavlc_consumed_bits_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/cavlc_consumed_bits_decoding.v} {./src}}} /designs/nova/subdesigns/cavlc_consumed_bits_decoding
::legacy::set_attribute -quiet arch_filename ./src/cavlc_consumed_bits_decoding.v /designs/nova/subdesigns/cavlc_consumed_bits_decoding
::legacy::set_attribute -quiet entity_filename ./src/cavlc_consumed_bits_decoding.v /designs/nova/subdesigns/cavlc_consumed_bits_decoding
::legacy::set_attribute -quiet hdl_user_name end_of_blk_decoding /designs/nova/subdesigns/end_of_blk_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/end_of_blk_decoding.v} {./src}}} /designs/nova/subdesigns/end_of_blk_decoding
::legacy::set_attribute -quiet arch_filename ./src/end_of_blk_decoding.v /designs/nova/subdesigns/end_of_blk_decoding
::legacy::set_attribute -quiet entity_filename ./src/end_of_blk_decoding.v /designs/nova/subdesigns/end_of_blk_decoding
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/level_decoding
::legacy::set_attribute -quiet hdl_user_name level_decoding /designs/nova/subdesigns/level_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/level_decoding.v} {./src}}} /designs/nova/subdesigns/level_decoding
::legacy::set_attribute -quiet arch_filename ./src/level_decoding.v /designs/nova/subdesigns/level_decoding
::legacy::set_attribute -quiet entity_filename ./src/level_decoding.v /designs/nova/subdesigns/level_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/cavlc_decoder/instances_hier/level_decoding/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/nC_decoding
::legacy::set_attribute -quiet hdl_user_name nC_decoding /designs/nova/subdesigns/nC_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/nC_decoding.v} {./src}}} /designs/nova/subdesigns/nC_decoding
::legacy::set_attribute -quiet arch_filename ./src/nC_decoding.v /designs/nova/subdesigns/nC_decoding
::legacy::set_attribute -quiet entity_filename ./src/nC_decoding.v /designs/nova/subdesigns/nC_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/cavlc_decoder/instances_hier/nC_decoding/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/run_decoding
::legacy::set_attribute -quiet hdl_user_name run_decoding /designs/nova/subdesigns/run_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/run_decoding.v} {./src}}} /designs/nova/subdesigns/run_decoding
::legacy::set_attribute -quiet arch_filename ./src/run_decoding.v /designs/nova/subdesigns/run_decoding
::legacy::set_attribute -quiet entity_filename ./src/run_decoding.v /designs/nova/subdesigns/run_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/cavlc_decoder/instances_hier/run_decoding/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/total_zeros_decoding
::legacy::set_attribute -quiet hdl_user_name total_zeros_decoding /designs/nova/subdesigns/total_zeros_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/total_zeros_decoding.v} {./src}}} /designs/nova/subdesigns/total_zeros_decoding
::legacy::set_attribute -quiet arch_filename ./src/total_zeros_decoding.v /designs/nova/subdesigns/total_zeros_decoding
::legacy::set_attribute -quiet entity_filename ./src/total_zeros_decoding.v /designs/nova/subdesigns/total_zeros_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/cavlc_decoder/instances_hier/total_zeros_decoding/nets/clk
::legacy::set_attribute -quiet hdl_user_name dependent_variable_decoding /designs/nova/subdesigns/dependent_variable_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/dependent_variable_decoding.v} {./src}}} /designs/nova/subdesigns/dependent_variable_decoding
::legacy::set_attribute -quiet arch_filename ./src/dependent_variable_decoding.v /designs/nova/subdesigns/dependent_variable_decoding
::legacy::set_attribute -quiet entity_filename ./src/dependent_variable_decoding.v /designs/nova/subdesigns/dependent_variable_decoding
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/exp_golomb_decoding
::legacy::set_attribute -quiet hdl_user_name exp_golomb_decoding /designs/nova/subdesigns/exp_golomb_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/exp_golomb_decoding.v} {./src}}} /designs/nova/subdesigns/exp_golomb_decoding
::legacy::set_attribute -quiet arch_filename ./src/exp_golomb_decoding.v /designs/nova/subdesigns/exp_golomb_decoding
::legacy::set_attribute -quiet entity_filename ./src/exp_golomb_decoding.v /designs/nova/subdesigns/exp_golomb_decoding
::legacy::set_attribute -quiet hdl_user_name heading_one_detector /designs/nova/subdesigns/heading_one_detector
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/heading_one_detector.v} {./src}}} /designs/nova/subdesigns/heading_one_detector
::legacy::set_attribute -quiet arch_filename ./src/heading_one_detector.v /designs/nova/subdesigns/heading_one_detector
::legacy::set_attribute -quiet entity_filename ./src/heading_one_detector.v /designs/nova/subdesigns/heading_one_detector
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11_8531
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11_8531
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11_8531
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width32_data_depth11_8531
::legacy::set_attribute -quiet hdl_user_name ram_async_1r_sync_1w /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10_8532
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_async_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10_8532
::legacy::set_attribute -quiet arch_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10_8532
::legacy::set_attribute -quiet entity_filename ./src/ram_async_1r_sync_1w.v /designs/nova/subdesigns/ram_async_1r_sync_1w_data_width8_data_depth10_8532
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/pc_decoding
::legacy::set_attribute -quiet hdl_user_name pc_decoding /designs/nova/subdesigns/pc_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/pc_decoding.v} {./src}}} /designs/nova/subdesigns/pc_decoding
::legacy::set_attribute -quiet arch_filename ./src/pc_decoding.v /designs/nova/subdesigns/pc_decoding
::legacy::set_attribute -quiet entity_filename ./src/pc_decoding.v /designs/nova/subdesigns/pc_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/pc_decoding/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/syntax_decoding
::legacy::set_attribute -quiet hdl_user_name syntax_decoding /designs/nova/subdesigns/syntax_decoding
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/syntax_decoding.v} {./src}}} /designs/nova/subdesigns/syntax_decoding
::legacy::set_attribute -quiet arch_filename ./src/syntax_decoding.v /designs/nova/subdesigns/syntax_decoding
::legacy::set_attribute -quiet entity_filename ./src/syntax_decoding.v /designs/nova/subdesigns/syntax_decoding
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/BitStream_controller/instances_hier/syntax_decoding/nets/clk
::legacy::set_attribute -quiet hdl_user_name reconstruction /designs/nova/subdesigns/reconstruction
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_mem_ctrl.v src/DF_pipeline.v src/DF_reg_ctrl.v src/DF_top.v src/ext_RAM_ctrl.v src/hybrid_pipeline_ctrl.v src/Inter_pred_CPE.v src/Inter_pred_LPE.v src/Inter_pred_pipeline.v src/Inter_pred_reg_ctrl.v src/Inter_pred_sliding_window.v src/Inter_pred_top.v src/Intra_pred_PE.v src/Intra_pred_pipeline.v src/Intra_pred_reg_ctrl.v src/Intra_pred_top.v src/IQIT.v src/ram_sync_1r_sync_1w.v src/rec_DF_RAM_ctrl.v src/rec_gclk_gen.v src/reconstruction.v src/sum.v} {./src}}} /designs/nova/subdesigns/reconstruction
::legacy::set_attribute -quiet arch_filename ./src/reconstruction.v /designs/nova/subdesigns/reconstruction
::legacy::set_attribute -quiet entity_filename ./src/reconstruction.v /designs/nova/subdesigns/reconstruction
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/nets/clk
::legacy::set_attribute -quiet hdl_user_name DF_top /designs/nova/subdesigns/DF_top
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_mem_ctrl.v src/DF_pipeline.v src/DF_reg_ctrl.v src/DF_top.v src/ram_sync_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/DF_top
::legacy::set_attribute -quiet arch_filename ./src/DF_top.v /designs/nova/subdesigns/DF_top
::legacy::set_attribute -quiet entity_filename ./src/DF_top.v /designs/nova/subdesigns/DF_top
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/DF_top/nets/clk
::legacy::set_attribute -quiet hdl_user_name ram_sync_1r_sync_1w /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth32
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_sync_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth32
::legacy::set_attribute -quiet arch_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth32
::legacy::set_attribute -quiet entity_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth32
::legacy::set_attribute -quiet hdl_user_name ram_sync_1r_sync_1w /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth352
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_sync_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth352
::legacy::set_attribute -quiet arch_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth352
::legacy::set_attribute -quiet entity_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth352
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/DF_mem_ctrl
::legacy::set_attribute -quiet hdl_user_name DF_mem_ctrl /designs/nova/subdesigns/DF_mem_ctrl
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_mem_ctrl.v} {./src}}} /designs/nova/subdesigns/DF_mem_ctrl
::legacy::set_attribute -quiet arch_filename ./src/DF_mem_ctrl.v /designs/nova/subdesigns/DF_mem_ctrl
::legacy::set_attribute -quiet entity_filename ./src/DF_mem_ctrl.v /designs/nova/subdesigns/DF_mem_ctrl
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/DF_top/instances_hier/DF_mem_ctrl/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_531_79_group_13622
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_538_94_group_13618
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/DF_pipeline
::legacy::set_attribute -quiet hdl_user_name DF_pipeline /designs/nova/subdesigns/DF_pipeline
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/DF_pipeline
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/DF_pipeline
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/DF_pipeline
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/DF_top/instances_hier/DF_pipeline/nets/clk
::legacy::set_attribute -quiet hdl_user_name absolute /designs/nova/subdesigns/absolute_7957
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/absolute_7957
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7957
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7957
::legacy::set_attribute -quiet hdl_user_name absolute /designs/nova/subdesigns/absolute_7956
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/absolute_7956
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7956
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7956
::legacy::set_attribute -quiet hdl_user_name absolute /designs/nova/subdesigns/absolute_7955
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/absolute_7955
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7955
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7955
::legacy::set_attribute -quiet hdl_user_name absolute /designs/nova/subdesigns/absolute
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/absolute
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute
::legacy::set_attribute -quiet hdl_user_name absolute /designs/nova/subdesigns/absolute_7959
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/absolute_7959
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7959
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7959
::legacy::set_attribute -quiet hdl_user_name absolute /designs/nova/subdesigns/absolute_7958
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/absolute_7958
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7958
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/absolute_7958
::legacy::set_attribute -quiet hdl_user_name bs4_strong_FIR /designs/nova/subdesigns/bs4_strong_FIR
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/bs4_strong_FIR
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/bs4_strong_FIR
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/bs4_strong_FIR
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_12828_1
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_12828_2
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_12828
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_839_26_group_13548
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_841_26_group_13552
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_842_26_group_13550
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_844_26_group_13554
::legacy::set_attribute -quiet hdl_user_name bs4_weak_FIR /designs/nova/subdesigns/bs4_weak_FIR
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/bs4_weak_FIR
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/bs4_weak_FIR
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/bs4_weak_FIR
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_858_35_group_13541_13542
::legacy::set_attribute -quiet hdl_user_name bs4_weak_FIR /designs/nova/subdesigns/bs4_weak_FIR_8374
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/bs4_weak_FIR_8374
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/bs4_weak_FIR_8374
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/bs4_weak_FIR_8374
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_858_35_group_13541_13542_24045
::legacy::set_attribute -quiet hdl_user_name clip_to_c /designs/nova/subdesigns/clip_to_c
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/clip_to_c
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/clip_to_c
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/clip_to_c
::legacy::set_attribute -quiet hdl_user_name clip_to_c /designs/nova/subdesigns/clip_to_c_8376
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/clip_to_c_8376
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/clip_to_c_8376
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/clip_to_c_8376
::legacy::set_attribute -quiet hdl_user_name clip_to_c /designs/nova/subdesigns/clip_to_c_8375
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_pipeline.v} {./src}}} /designs/nova/subdesigns/clip_to_c_8375
::legacy::set_attribute -quiet arch_filename ./src/DF_pipeline.v /designs/nova/subdesigns/clip_to_c_8375
::legacy::set_attribute -quiet entity_filename ./src/DF_pipeline.v /designs/nova/subdesigns/clip_to_c_8375
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_556_97_group_13588
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_575_62_group_13584
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_62_group_13586
::legacy::set_attribute -quiet hdl_user_name DF_reg_ctrl /designs/nova/subdesigns/DF_reg_ctrl
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/DF_reg_ctrl.v} {./src}}} /designs/nova/subdesigns/DF_reg_ctrl
::legacy::set_attribute -quiet arch_filename ./src/DF_reg_ctrl.v /designs/nova/subdesigns/DF_reg_ctrl
::legacy::set_attribute -quiet entity_filename ./src/DF_reg_ctrl.v /designs/nova/subdesigns/DF_reg_ctrl
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/IQIT
::legacy::set_attribute -quiet hdl_user_name IQIT /designs/nova/subdesigns/IQIT
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/IQIT
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/IQIT
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/IQIT
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/nets/clk
::legacy::set_attribute -quiet hdl_user_name butterfly /designs/nova/subdesigns/butterfly
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/butterfly
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/butterfly
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/butterfly
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_787_17_group_13558
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_788_17_group_13534
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_sub_789_17_group_13536
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_sub_790_17_group_13538
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/div6
::legacy::set_attribute -quiet hdl_user_name div6 /designs/nova/subdesigns/div6
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/div6
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/div6
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/div6
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/mod6
::legacy::set_attribute -quiet hdl_user_name mod6 /designs/nova/subdesigns/mod6
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/mod6
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/mod6
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/mod6
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/mult_unsigned
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 47 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/mul_558_54
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/mult_unsigned_23816
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 47 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/mul_559_54
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/mult_unsigned_23817
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 47 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/mul_560_54
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/mult_unsigned_23815
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 47 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/mul_561_54
::legacy::set_attribute -quiet hdl_user_name rescale_shift /designs/nova/subdesigns/rescale_shift
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/rescale_shift
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/shift_left_vlog_unsigned_5931
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 21 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/rescale_shift0/instances_hier/sll_834_60
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/arith_shift_right_vlog_unsigned
::legacy::set_attribute -quiet hdl_user_name rescale_shift /designs/nova/subdesigns/rescale_shift_8536
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/rescale_shift_8536
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift_8536
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift_8536
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/shift_left_vlog_unsigned_5931_23950
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 21 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/rescale_shift1/instances_hier/sll_834_60
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/arith_shift_right_vlog_unsigned_23884
::legacy::set_attribute -quiet hdl_user_name rescale_shift /designs/nova/subdesigns/rescale_shift_8535
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/rescale_shift_8535
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift_8535
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift_8535
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/shift_left_vlog_unsigned_5931_23951
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 21 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/rescale_shift2/instances_hier/sll_834_60
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/arith_shift_right_vlog_unsigned_23885
::legacy::set_attribute -quiet hdl_user_name rescale_shift /designs/nova/subdesigns/rescale_shift_8534
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/IQIT.v} {./src}}} /designs/nova/subdesigns/rescale_shift_8534
::legacy::set_attribute -quiet arch_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift_8534
::legacy::set_attribute -quiet entity_filename ./src/IQIT.v /designs/nova/subdesigns/rescale_shift_8534
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/shift_left_vlog_unsigned_5931_23949
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 21 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/IQIT/instances_hier/rescale_shift3/instances_hier/sll_834_60
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/arith_shift_right_vlog_unsigned_23883
::legacy::set_attribute -quiet hdl_user_name Inter_pred_top /designs/nova/subdesigns/Inter_pred_top
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v src/Inter_pred_LPE.v src/Inter_pred_pipeline.v src/Inter_pred_reg_ctrl.v src/Inter_pred_sliding_window.v src/Inter_pred_top.v} {./src}}} /designs/nova/subdesigns/Inter_pred_top
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_top.v /designs/nova/subdesigns/Inter_pred_top
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_top.v /designs/nova/subdesigns/Inter_pred_top
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/nets/clk
::legacy::set_attribute -quiet hdl_user_name Inter_pred_CPE /designs/nova/subdesigns/Inter_pred_CPE
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/Inter_pred_CPE
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/Inter_pred_CPE
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/Inter_pred_CPE
::legacy::set_attribute -quiet hdl_user_name CPE /designs/nova/subdesigns/CPE
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15_23774
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_15
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7951
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7951
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7951
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7951
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_7
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7950
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7950
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7950
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7950
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_13
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7949
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7949
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7949
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7949
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_23767
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_10
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_112_85_group_13693_13694_3
::legacy::set_attribute -quiet hdl_user_name CPE /designs/nova/subdesigns/CPE_7936
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_7936
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_7936
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_7936
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7948
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7948
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7948
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7948
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15_23768
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_8
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7947
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7947
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7947
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7947
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_23764
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_4
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7946
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7946
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7946
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7946
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15_23773
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_13_23954
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7945
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7945
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7945
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7945
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_23766
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_10_23956
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_112_85_group_13693_13694
::legacy::set_attribute -quiet hdl_user_name CPE /designs/nova/subdesigns/CPE_7935
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_7935
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_7935
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_7935
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7944
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7944
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7944
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7944
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15_23772
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_11
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7943
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7943
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7943
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7943
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_23763
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_3
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7942
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7942
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7942
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7942
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15_23771
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_13_23953
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7941
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7941
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7941
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7941
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_23765
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_10_23957
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_112_85_group_13693_13694_1
::legacy::set_attribute -quiet hdl_user_name CPE /designs/nova/subdesigns/CPE_7934
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_7934
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_7934
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_7934
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7940
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7940
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7940
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7940
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15_23770
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_12
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7939
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7939
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7939
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7939
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_23762
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7938
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7938
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7938
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7938
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_15_23769
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_13_23952
::legacy::set_attribute -quiet hdl_user_name CPE_base /designs/nova/subdesigns/CPE_base_7937
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_CPE.v} {./src}}} /designs/nova/subdesigns/CPE_base_7937
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7937
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_CPE.v /designs/nova/subdesigns/CPE_base_7937
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_137_35_group_13653_13654_23761
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_143_33_group_13685_13686_10_23955
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_112_85_group_13693_13694_2
::legacy::set_attribute -quiet hdl_user_name Inter_pred_LPE /designs/nova/subdesigns/Inter_pred_LPE
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/Inter_pred_LPE
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/Inter_pred_LPE
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/Inter_pred_LPE
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/nets/clk
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter0/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8390
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8390
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8390
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8390
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_7
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24055
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter1/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8389
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8389
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8389
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8389
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_4
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_545_46_group_13531_13532
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24054
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter2/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8388
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8388
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8388
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8388
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_5
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_545_46_group_13531_13532_24033
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24053
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter3/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8387
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8387
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8387
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8387
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_6
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_545_46_group_13531_13532_24034
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24052
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter4/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8386
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8386
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8386
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8386
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_3
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_545_46_group_13531_13532_24032
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24051
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter5/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8385
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8385
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8385
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8385
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_2
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_545_46_group_13531_13532_5
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24050
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter6/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8384
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8384
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8384
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8384
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_1
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24049
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter7/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterH_6tap /designs/nova/subdesigns/filterH_6tap_8383
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterH_6tap_8383
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8383
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterH_6tap_8383
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_13198_8
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_24048
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/H_6tapfilter8/instances_hier/final_adder_add_543_42
::legacy::set_attribute -quiet hdl_user_name filterV_6tap /designs/nova/subdesigns/filterV_6tap
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterV_6tap
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457_7969
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter0/instances_hier/add_568_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter0/instances_hier/add_569_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_572_42_group_13513_13514_3
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_42_group_13505_13506_3
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_58_group_13497_13498
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_unsigned_13251
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 7 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter0/instances_hier/final_adder_add_571_61
::legacy::set_attribute -quiet hdl_user_name filterV_6tap /designs/nova/subdesigns/filterV_6tap_8393
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterV_6tap_8393
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap_8393
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap_8393
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457_7969_24068
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter1/instances_hier/add_568_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457_24063
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter1/instances_hier/add_569_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_572_42_group_13513_13514
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_42_group_13505_13506
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_58_group_13497_13498_23999
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_unsigned_13251_2
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 7 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter1/instances_hier/final_adder_add_571_61
::legacy::set_attribute -quiet hdl_user_name filterV_6tap /designs/nova/subdesigns/filterV_6tap_8392
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterV_6tap_8392
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap_8392
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap_8392
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457_7969_24067
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter2/instances_hier/add_568_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457_24062
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter2/instances_hier/add_569_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_572_42_group_13513_13514_1
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_42_group_13505_13506_1
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_58_group_13497_13498_24000
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_unsigned_13251_1
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 7 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter2/instances_hier/final_adder_add_571_61
::legacy::set_attribute -quiet hdl_user_name filterV_6tap /designs/nova/subdesigns/filterV_6tap_8391
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/filterV_6tap_8391
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap_8391
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/filterV_6tap_8391
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457_7969_24066
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter3/instances_hier/add_568_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_signed_15457_24061
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 1 1 2 1 1 2 0 1} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter3/instances_hier/add_569_28
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_572_42_group_13513_13514_2
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_42_group_13505_13506_2
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_576_58_group_13497_13498_24001
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/add_unsigned_13251_3
::legacy::set_attribute -quiet rtlop_info {{} 0 0 0 3 0 7 0 2 1 1 0} /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_LPE/instances_hier/V_6tapfilter3/instances_hier/final_adder_add_571_61
::legacy::set_attribute -quiet hdl_user_name bilinear /designs/nova/subdesigns/bilinear
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/bilinear
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear
::legacy::set_attribute -quiet hdl_user_name bilinear /designs/nova/subdesigns/bilinear_8373
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/bilinear_8373
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear_8373
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear_8373
::legacy::set_attribute -quiet hdl_user_name bilinear /designs/nova/subdesigns/bilinear_8372
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/bilinear_8372
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear_8372
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear_8372
::legacy::set_attribute -quiet hdl_user_name bilinear /designs/nova/subdesigns/bilinear_8371
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_LPE.v} {./src}}} /designs/nova/subdesigns/bilinear_8371
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear_8371
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_LPE.v /designs/nova/subdesigns/bilinear_8371
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/Inter_pred_pipeline
::legacy::set_attribute -quiet hdl_user_name Inter_pred_pipeline /designs/nova/subdesigns/Inter_pred_pipeline
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_pipeline.v} {./src}}} /designs/nova/subdesigns/Inter_pred_pipeline
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_pipeline.v /designs/nova/subdesigns/Inter_pred_pipeline
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_pipeline.v /designs/nova/subdesigns/Inter_pred_pipeline
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Inter_pred_top/instances_hier/Inter_pred_pipeline/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_544_44_group_13572
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_700_75_group_13578
::legacy::set_attribute -quiet hdl_user_name Inter_pred_reg_ctrl /designs/nova/subdesigns/Inter_pred_reg_ctrl
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_reg_ctrl.v} {./src}}} /designs/nova/subdesigns/Inter_pred_reg_ctrl
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_reg_ctrl.v /designs/nova/subdesigns/Inter_pred_reg_ctrl
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_reg_ctrl.v /designs/nova/subdesigns/Inter_pred_reg_ctrl
::legacy::set_attribute -quiet hdl_user_name Inter_pred_sliding_window /designs/nova/subdesigns/Inter_pred_sliding_window
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Inter_pred_sliding_window.v} {./src}}} /designs/nova/subdesigns/Inter_pred_sliding_window
::legacy::set_attribute -quiet arch_filename ./src/Inter_pred_sliding_window.v /designs/nova/subdesigns/Inter_pred_sliding_window
::legacy::set_attribute -quiet entity_filename ./src/Inter_pred_sliding_window.v /designs/nova/subdesigns/Inter_pred_sliding_window
::legacy::set_attribute -quiet hdl_user_name Intra_pred_top /designs/nova/subdesigns/Intra_pred_top
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_PE.v src/Intra_pred_pipeline.v src/Intra_pred_reg_ctrl.v src/Intra_pred_top.v src/ram_sync_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/Intra_pred_top
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_top.v /designs/nova/subdesigns/Intra_pred_top
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_top.v /designs/nova/subdesigns/Intra_pred_top
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Intra_pred_top/nets/clk
::legacy::set_attribute -quiet hdl_user_name ram_sync_1r_sync_1w /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth88
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_sync_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth88
::legacy::set_attribute -quiet arch_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth88
::legacy::set_attribute -quiet entity_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth88
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/Intra_pred_PE
::legacy::set_attribute -quiet hdl_user_name Intra_pred_PE /designs/nova/subdesigns/Intra_pred_PE
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_PE.v} {./src}}} /designs/nova/subdesigns/Intra_pred_PE
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/Intra_pred_PE
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/Intra_pred_PE
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Intra_pred_top/instances_hier/Intra_pred_PE/nets/clk
::legacy::set_attribute -quiet hdl_user_name PE /designs/nova/subdesigns/PE
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_PE.v} {./src}}} /designs/nova/subdesigns/PE
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Intra_pred_top/instances_hier/Intra_pred_PE/instances_hier/PE0/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_1613_42_group_13565_13566_3
::legacy::set_attribute -quiet hdl_user_name PE /designs/nova/subdesigns/PE_7954
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_PE.v} {./src}}} /designs/nova/subdesigns/PE_7954
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE_7954
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE_7954
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Intra_pred_top/instances_hier/Intra_pred_PE/instances_hier/PE1/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_1613_42_group_13565_13566
::legacy::set_attribute -quiet hdl_user_name PE /designs/nova/subdesigns/PE_7953
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_PE.v} {./src}}} /designs/nova/subdesigns/PE_7953
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE_7953
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE_7953
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Intra_pred_top/instances_hier/Intra_pred_PE/instances_hier/PE2/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_1613_42_group_13565_13566_1
::legacy::set_attribute -quiet hdl_user_name PE /designs/nova/subdesigns/PE_7952
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_PE.v} {./src}}} /designs/nova/subdesigns/PE_7952
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE_7952
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_PE.v /designs/nova/subdesigns/PE_7952
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Intra_pred_top/instances_hier/Intra_pred_PE/instances_hier/PE3/nets/clk
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_1613_42_group_13565_13566_2
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/Intra_pred_pipeline
::legacy::set_attribute -quiet hdl_user_name Intra_pred_pipeline /designs/nova/subdesigns/Intra_pred_pipeline
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_pipeline.v} {./src}}} /designs/nova/subdesigns/Intra_pred_pipeline
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/Intra_pred_pipeline
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/Intra_pred_pipeline
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/Intra_pred_top/instances_hier/Intra_pred_pipeline/nets/clk
::legacy::set_attribute -quiet hdl_user_name main_seed_precomputation /designs/nova/subdesigns/main_seed_precomputation
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_pipeline.v} {./src}}} /designs/nova/subdesigns/main_seed_precomputation
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/main_seed_precomputation
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/main_seed_precomputation
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add0022766_group_13488
::legacy::set_attribute -quiet hdl_user_name plane_HV_precomputation /designs/nova/subdesigns/plane_HV_precomputation
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_pipeline.v} {./src}}} /designs/nova/subdesigns/plane_HV_precomputation
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/plane_HV_precomputation
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/plane_HV_precomputation
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_694_29_group_13486
::legacy::set_attribute -quiet hdl_user_name plane_a_precomputation /designs/nova/subdesigns/plane_a_precomputation
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_pipeline.v} {./src}}} /designs/nova/subdesigns/plane_a_precomputation
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/plane_a_precomputation
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/plane_a_precomputation
::legacy::set_attribute -quiet hdl_user_name plane_bc_precomputation /designs/nova/subdesigns/plane_bc_precomputation
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_pipeline.v} {./src}}} /designs/nova/subdesigns/plane_bc_precomputation
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/plane_bc_precomputation
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_pipeline.v /designs/nova/subdesigns/plane_bc_precomputation
::legacy::set_attribute -quiet logical_hier false /designs/nova/subdesigns/csa_tree_add_657_23_group_13484
::legacy::set_attribute -quiet hdl_user_name Intra_pred_reg_ctrl /designs/nova/subdesigns/Intra_pred_reg_ctrl
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/Intra_pred_reg_ctrl.v} {./src}}} /designs/nova/subdesigns/Intra_pred_reg_ctrl
::legacy::set_attribute -quiet arch_filename ./src/Intra_pred_reg_ctrl.v /designs/nova/subdesigns/Intra_pred_reg_ctrl
::legacy::set_attribute -quiet entity_filename ./src/Intra_pred_reg_ctrl.v /designs/nova/subdesigns/Intra_pred_reg_ctrl
::legacy::set_attribute -quiet hdl_user_name ext_RAM_ctrl /designs/nova/subdesigns/ext_RAM_ctrl
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ext_RAM_ctrl.v} {./src}}} /designs/nova/subdesigns/ext_RAM_ctrl
::legacy::set_attribute -quiet arch_filename ./src/ext_RAM_ctrl.v /designs/nova/subdesigns/ext_RAM_ctrl
::legacy::set_attribute -quiet entity_filename ./src/ext_RAM_ctrl.v /designs/nova/subdesigns/ext_RAM_ctrl
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/ext_RAM_ctrl/nets/clk
::legacy::set_attribute -quiet hdl_user_name hybrid_pipeline_ctrl /designs/nova/subdesigns/hybrid_pipeline_ctrl
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/hybrid_pipeline_ctrl.v} {./src}}} /designs/nova/subdesigns/hybrid_pipeline_ctrl
::legacy::set_attribute -quiet arch_filename ./src/hybrid_pipeline_ctrl.v /designs/nova/subdesigns/hybrid_pipeline_ctrl
::legacy::set_attribute -quiet entity_filename ./src/hybrid_pipeline_ctrl.v /designs/nova/subdesigns/hybrid_pipeline_ctrl
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/hybrid_pipeline_ctrl/nets/clk
::legacy::set_attribute -quiet hdl_user_name ram_sync_1r_sync_1w /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_sync_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96
::legacy::set_attribute -quiet arch_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96
::legacy::set_attribute -quiet entity_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96
::legacy::set_attribute -quiet hdl_user_name ram_sync_1r_sync_1w /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96_8533
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/ram_sync_1r_sync_1w.v} {./src}}} /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96_8533
::legacy::set_attribute -quiet arch_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96_8533
::legacy::set_attribute -quiet entity_filename ./src/ram_sync_1r_sync_1w.v /designs/nova/subdesigns/ram_sync_1r_sync_1w_data_width32_data_depth96_8533
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/rec_DF_RAM_ctrl
::legacy::set_attribute -quiet hdl_user_name rec_DF_RAM_ctrl /designs/nova/subdesigns/rec_DF_RAM_ctrl
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/rec_DF_RAM_ctrl.v} {./src}}} /designs/nova/subdesigns/rec_DF_RAM_ctrl
::legacy::set_attribute -quiet arch_filename ./src/rec_DF_RAM_ctrl.v /designs/nova/subdesigns/rec_DF_RAM_ctrl
::legacy::set_attribute -quiet entity_filename ./src/rec_DF_RAM_ctrl.v /designs/nova/subdesigns/rec_DF_RAM_ctrl
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/rec_DF_RAM_ctrl/nets/clk
::legacy::set_attribute -quiet hdl_user_name rec_gclk_gen /designs/nova/subdesigns/rec_gclk_gen
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/rec_gclk_gen.v} {./src}}} /designs/nova/subdesigns/rec_gclk_gen
::legacy::set_attribute -quiet arch_filename ./src/rec_gclk_gen.v /designs/nova/subdesigns/rec_gclk_gen
::legacy::set_attribute -quiet entity_filename ./src/rec_gclk_gen.v /designs/nova/subdesigns/rec_gclk_gen
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/rec_gclk_gen/nets/clk
::legacy::set_attribute -quiet is_sop_cluster true /designs/nova/subdesigns/sum
::legacy::set_attribute -quiet hdl_user_name sum /designs/nova/subdesigns/sum
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/sum.v} {./src}}} /designs/nova/subdesigns/sum
::legacy::set_attribute -quiet arch_filename ./src/sum.v /designs/nova/subdesigns/sum
::legacy::set_attribute -quiet entity_filename ./src/sum.v /designs/nova/subdesigns/sum
::legacy::set_attribute -quiet preserve true /designs/nova/instances_hier/reconstruction/instances_hier/sum/nets/clk
::legacy::set_attribute -quiet hdl_user_name sum_PE /designs/nova/subdesigns/sum_PE
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/sum.v} {./src}}} /designs/nova/subdesigns/sum_PE
::legacy::set_attribute -quiet arch_filename ./src/sum.v /designs/nova/subdesigns/sum_PE
::legacy::set_attribute -quiet entity_filename ./src/sum.v /designs/nova/subdesigns/sum_PE
::legacy::set_attribute -quiet hdl_user_name sum_PE /designs/nova/subdesigns/sum_PE_8586
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/sum.v} {./src}}} /designs/nova/subdesigns/sum_PE_8586
::legacy::set_attribute -quiet arch_filename ./src/sum.v /designs/nova/subdesigns/sum_PE_8586
::legacy::set_attribute -quiet entity_filename ./src/sum.v /designs/nova/subdesigns/sum_PE_8586
::legacy::set_attribute -quiet hdl_user_name sum_PE /designs/nova/subdesigns/sum_PE_8585
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/sum.v} {./src}}} /designs/nova/subdesigns/sum_PE_8585
::legacy::set_attribute -quiet arch_filename ./src/sum.v /designs/nova/subdesigns/sum_PE_8585
::legacy::set_attribute -quiet entity_filename ./src/sum.v /designs/nova/subdesigns/sum_PE_8585
::legacy::set_attribute -quiet hdl_user_name sum_PE /designs/nova/subdesigns/sum_PE_8584
::legacy::set_attribute -quiet hdl_filelist {{default -v2001 {SYNTHESIS} {src/sum.v} {./src}}} /designs/nova/subdesigns/sum_PE_8584
::legacy::set_attribute -quiet arch_filename ./src/sum.v /designs/nova/subdesigns/sum_PE_8584
::legacy::set_attribute -quiet entity_filename ./src/sum.v /designs/nova/subdesigns/sum_PE_8584
