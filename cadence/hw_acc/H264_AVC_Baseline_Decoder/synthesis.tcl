## Template para síntese lógica no Cadence GENUS
## Autor: Marcelo Brandalero
##
##
## Data: 14.08.2018
##
## Modificado por: Pedro
##
## Data: 22.11.2018
##
## Modificado por: Pedro
##
## Data: 07.03.2019
## 
## Modificações: modificado para H264 like

set DESIGN_NAME [basename [lpwd]]

# Seu design aqui - arquivos que serão lidos
set_attr hdl_search_path ./src
set DESIGNS {Beha_BitStream_ram.v BitStream_buffer.v BitStream_controller.v bitstream_gclk_gen.v BitStream_parser_FSM_gating.v bs_decoding.v cavlc_consumed_bits_decoding.v cavlc_decoder.v CodedBlockPattern_decoding.v dependent_variable_decoding.v DF_mem_ctrl.v DF_pipeline.v DF_reg_ctrl.v DF_top.v end_of_blk_decoding.v exp_golomb_decoding.v ext_frame_RAM0_wrapper.v ext_frame_RAM1_wrapper.v ext_RAM_ctrl.v heading_one_detector.v hybrid_pipeline_ctrl.v Inter_mv_decoding.v Inter_pred_CPE.v Inter_pred_LPE.v Inter_pred_pipeline.v Inter_pred_reg_ctrl.v Inter_pred_sliding_window.v Inter_pred_top.v Intra4x4_PredMode_decoding.v Intra_pred_PE.v Intra_pred_pipeline.v Intra_pred_reg_ctrl.v Intra_pred_top.v IQIT.v level_decoding.v nC_decoding.v nova_defines.v nova_tb.v nova.v NumCoeffTrailingOnes_decoding.v pc_decoding.v QP_decoding.v ram_async_1r_sync_1w.v ram_sync_1r_sync_1w.v rec_DF_RAM_ctrl.v rec_gclk_gen.v reconstruction.v run_decoding.v sum.v syntax_decoding.v timescale.v total_zeros_decoding.v}
# Seu modulo top aqui
set MODULE_NAME "nova"
# Isso aqui é pra fazer análise de potencia - o vcd é o waveform que é carregado antes da síntese pra estimar o chaveamento e potencia das coisas, e consequentemente ele ir otimizando durante a sintese. tem que tirar fora se não tiver vcd
#set VCD_PATH "/workareas/marcelo.brandalero/Rocket.vcd"

# Pedro: no meu caso uso o saif, que é menor.
# Não uso o saif nesse caso, só quero área para o trabalho do partial isa
#set SAIF_PATH "saif/${DESIGN_NAME}.saif"

#clock delay
#set CLOCK_DELAY 2000
set CLOCK_DELAY 5000
#200MHz

# auto-explicativo
# o map-effort tem que ser no maximo medium se tu não quiser que ele desagrupe os modulos. se colocar em high tu tens um design otimizado, mas não vais conseguir fazer o breakdown da potencia/area das coisas


set SYN_EFF medium
set MAP_EFF medium

# caminho da lib
set_attribute lib_search_path /home/gme/pedro.becker/NanGate_15nm_OCL_v0.1_2014_06.A/front_end/timing_power_noise/CCS/
# nome do arquivo da lib (dentro do caminho)
set_attribute library NanGate_15nm_OCL_typical_conditional_ccs.lib

# serviria pra usar o mesmo modelo de fio (capacitancias) pra todo o circuito, mas a biblioteca que eu uso não tem isso
# se a biblioteca que tu usar tiver, tem que setar o atributo wireload_MODEL (nota que é outro) pro nome do modelo
set_attribute wireload_mode top
# lp são os atributos relacionados à analise de potencia
# default_probability é a probabilidade de uma rede estar em nível lógico 1
# tem que setar também o lp_default_toggle_rate, que é dado em chaveamentos/ns (ou ps, dependendo da lib) - aqui eu não setei pq a análise era feita em cima do VCD
set_attribute lp_default_probability 0.5
# auto explicativo
set_attribute lp_power_analysis_effort high

# aqui o toggle rate que falei acima
# a conta se refere a "metade do chavemento maximo" - nota que ele chaveia 0.5 vezes a cada ciclo de clock
set_attribute lp_default_toggle_rate [expr 0.5 * double(1000) / ${CLOCK_DELAY}]

# auto explicativo
read_hdl $DESIGNS
puts "Runtime & Memory after 'read_hdl'"
timestat

# transforma os hdl em uma representação na memoria
elaborate $MODULE_NAME
puts "Runtime & Memory after 'elaborate'"
timestat

# le o vcd - se não tiver, comenta fora
#read_vcd -scale [expr double(${CLOCK_DELAY}) / 2000] -static -ignorecase -vcd_scope core -start_time 400000000 -end_time 450000000 ${VCD_PATH}

# le o saif
#read_saif -scale [expr double(${CLOCK_DELAY}) / 2000] -instance BoomCore $SAIF_PATH

# pra otimizar pra potencia dinamica, seta max potencia dinamica = 0
set_attribute max_dynamic_power 0 [find / -des *]

# isso aqui é pra ele tratar as redes de clock como ideais, sem delay
# a geração da árvore de clock, que minimiza os delays dessa rede, é feita apenas durante o place & route, que é posterior à sintese logica que é feita aqui
create_clock -period ${CLOCK_DELAY} clk
set_ideal_network clk
set_dont_touch_network clk

# síntese "to generic" - quer dizer, gera netlist independente da tecnologia
synthesize -to_generic -eff ${SYN_EFF} ${MODULE_NAME}
puts "Runtime & Memory after 'synthesize -to_generic'"
timestat

# mapeia o netlist gerado para a biblioteca de células
synthesize -to_mapped  -eff ${MAP_EFF} ${MODULE_NAME}
puts "Runtime & Memory after 'synthesize -to_mapped'"
timestat

# auto explicativo - dá pra passar as opções "-detail" aqui
report timing  > reports/timing/timing_${DESIGN_NAME}_default.txt
report area    > reports/area/area_${DESIGN_NAME}_default.txt
report power   > reports/power/power_${DESIGN_NAME}_default.txt

# auto explicativo - o -tcf gera um arquivo com atividade de chaveamento, se tu for usar um vcd ou coisa parecida
write_design -basename designs/${DESIGN_NAME} -hierarchical -tcf

quit
