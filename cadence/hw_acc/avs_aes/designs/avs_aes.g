######################################################################

# Created by Genus(TM) Synthesis Solution GENUS15.22 - 15.20-s024_1 on Wed Aug 28 21:53:24 -0300 2019

# This file contains the RC script for /designs/avs_AES

######################################################################

::legacy::set_attribute -quiet init_lib_search_path /home/gme/pedro.becker/NanGate_15nm_OCL_v0.1_2014_06.A/front_end/timing_power_noise/CCS/ /
::legacy::set_attribute -quiet common_ui false /
::legacy::set_attribute -quiet design_mode_process no_value /
::legacy::set_attribute -quiet phys_assume_met_fill 0.0 /
::legacy::set_attribute -quiet runtime_by_stage { {to_generic 5 84 4 17}  {first_condense 57 149 62 87}  {reify 24 173 42 129}  {global_incr_map 18 195 16 150}  {incr_opt 12 212 10 165} } /
::legacy::set_attribute -quiet tinfo_tstamp_file .rs_pedro.becker.tstamp /
::legacy::set_attribute -quiet wireload_mode top /
::legacy::set_attribute -quiet lp_power_analysis_effort high /
::legacy::set_attribute -quiet lp_default_toggle_rate 0.10000 /
::legacy::set_attribute -quiet super_thread_servers {localhost localhost localhost localhost localhost localhost localhost localhost   } /
::legacy::set_attribute -quiet phys_use_segment_parasitics true /
::legacy::set_attribute -quiet probabilistic_extraction true /
::legacy::set_attribute -quiet ple_correlation_factors {1.9000 2.0000} /
::legacy::set_attribute -quiet maximum_interval_of_vias infinity /
::legacy::set_attribute -quiet interconnect_mode wireload /
::legacy::set_attribute -quiet wireload_mode top /
::legacy::set_attribute -quiet tree_type balanced_tree /libraries/NanGate_15nm_OCL/operating_conditions/typical
::legacy::set_attribute -quiet tree_type balanced_tree /libraries/NanGate_15nm_OCL/operating_conditions/_nominal_
# BEGIN MSV SECTION
# END MSV SECTION
define_clock -name clk -domain domain_1 -period 5000.0 -divide_period 1 -rise 0 -divide_rise 1 -fall 1 -divide_fall 2 -design /designs/avs_AES /designs/avs_AES/ports_in/clk
::legacy::set_attribute -quiet waveform { } /designs/avs_AES/timing/clock_domains/domain_1/clk
define_cost_group -design /designs/avs_AES -name clk
external_delay -accumulate -input {0.0 no_value 0.0 no_value} -clock /designs/avs_AES/timing/clock_domains/domain_1/clk -name create_clock_delay_domain_1_clk_R_0 /designs/avs_AES/ports_in/clk
::legacy::set_attribute -quiet clock_network_latency_included true /designs/avs_AES/timing/external_delays/create_clock_delay_domain_1_clk_R_0
external_delay -accumulate -input {no_value 0.0 no_value 0.0} -clock /designs/avs_AES/timing/clock_domains/domain_1/clk -edge_fall -name create_clock_delay_domain_1_clk_F_0 /designs/avs_AES/ports_in/clk
::legacy::set_attribute -quiet clock_network_latency_included true /designs/avs_AES/timing/external_delays/create_clock_delay_domain_1_clk_F_0
path_group -paths [specify_paths -to /designs/avs_AES/timing/clock_domains/domain_1/clk]  -name clk -group /designs/avs_AES/timing/cost_groups/clk -user_priority -1047552
# BEGIN DFT SECTION
::legacy::set_attribute -quiet dft_scan_style muxed_scan /
::legacy::set_attribute -quiet dft_scanbit_waveform_analysis false /
# END DFT SECTION
::legacy::set_attribute -quiet qos_by_stage {{to_generic {wns -11111111} {tns -111111111} {vep -111111111} {area 6534} {cell_count 6405} {utilization  0.00} {runtime 5 84 4 17} }{first_condense {wns -11111111} {tns -111111111} {vep -111111111} {area 9161} {cell_count 16020} {utilization  0.00} {runtime 57 149 62 87} }{reify {wns 4700} {tns 0} {vep 0} {area 6568} {cell_count 13569} {utilization  0.00} {runtime 24 174 42 130} }{global_incr_map {wns 4669} {tns 0} {vep 0} {area 6534} {cell_count 13496} {utilization  0.00} {runtime 18 195 16 151} }{incr_opt {wns 214748365} {tns 0} {vep 0} {area 6532} {cell_count 13485} {utilization  0.00} {runtime 12 212 10 165} }} /designs/avs_AES
::legacy::set_attribute -quiet is_sop_cluster true /designs/avs_AES
::legacy::set_attribute -quiet hdl_user_name avs_AES /designs/avs_AES
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mux2.vhd src/mux3.vhd src/memory_word.vhd src/addroundkey.vhd src/aes_fsm_encrypt.vhd src/aes_fsm_decrypt.vhd src/keyexpansionV2.vhd src/mixcol.vhd src/mixcol_fwd.vhd src/mixcol_inv.vhd src/sbox.vhd src/sboxM4k.vhd src/shiftrow.vhd src/shiftrow_fwd.vhd src/shiftrow_inv.vhd src/aes_core.vhd src/avs_aes.vhd} {./src}}} /designs/avs_AES
::legacy::set_attribute -quiet verification_directory fv/avs_AES /designs/avs_AES
::legacy::set_attribute -quiet max_dynamic_power 0.0 /designs/avs_AES
::legacy::set_attribute -quiet arch_filename ./src/avs_aes.vhd /designs/avs_AES
::legacy::set_attribute -quiet entity_filename ./src/avs_aes.vhd /designs/avs_AES
::legacy::set_attribute -quiet is_ideal_network true /designs/avs_AES/ports_in/clk
::legacy::set_attribute -quiet ideal_network true /designs/avs_AES/ports_in/clk
::legacy::set_attribute -quiet preserve true /designs/avs_AES/nets/clk
::legacy::set_attribute -quiet hdl_user_name AES_CORE /designs/avs_AES/subdesigns/AES_CORE_KEYLENGTH256_DECRYPTION1
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mux2.vhd src/mux3.vhd src/memory_word.vhd src/addroundkey.vhd src/aes_fsm_encrypt.vhd src/aes_fsm_decrypt.vhd src/keyexpansionV2.vhd src/mixcol.vhd src/mixcol_fwd.vhd src/mixcol_inv.vhd src/sbox.vhd src/sboxM4k.vhd src/shiftrow.vhd src/shiftrow_fwd.vhd src/shiftrow_inv.vhd src/aes_core.vhd} {./src}}} /designs/avs_AES/subdesigns/AES_CORE_KEYLENGTH256_DECRYPTION1
::legacy::set_attribute -quiet arch_filename ./src/aes_core.vhd /designs/avs_AES/subdesigns/AES_CORE_KEYLENGTH256_DECRYPTION1
::legacy::set_attribute -quiet entity_filename ./src/aes_core.vhd /designs/avs_AES/subdesigns/AES_CORE_KEYLENGTH256_DECRYPTION1
::legacy::set_attribute -quiet preserve true /designs/avs_AES/instances_hier/AES_CORE_1/nets/clk
::legacy::set_attribute -quiet hdl_user_name mux3 /designs/avs_AES/subdesigns/mux3_IOwidth32
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux3.vhd} {./src}}} /designs/avs_AES/subdesigns/mux3_IOwidth32
::legacy::set_attribute -quiet arch_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32
::legacy::set_attribute -quiet entity_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32
::legacy::set_attribute -quiet hdl_user_name mux3 /designs/avs_AES/subdesigns/mux3_IOwidth32_207
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux3.vhd} {./src}}} /designs/avs_AES/subdesigns/mux3_IOwidth32_207
::legacy::set_attribute -quiet arch_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32_207
::legacy::set_attribute -quiet entity_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32_207
::legacy::set_attribute -quiet hdl_user_name mux3 /designs/avs_AES/subdesigns/mux3_IOwidth32_206
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux3.vhd} {./src}}} /designs/avs_AES/subdesigns/mux3_IOwidth32_206
::legacy::set_attribute -quiet arch_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32_206
::legacy::set_attribute -quiet entity_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32_206
::legacy::set_attribute -quiet hdl_user_name mux3 /designs/avs_AES/subdesigns/mux3_IOwidth32_205
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux3.vhd} {./src}}} /designs/avs_AES/subdesigns/mux3_IOwidth32_205
::legacy::set_attribute -quiet arch_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32_205
::legacy::set_attribute -quiet entity_filename ./src/mux3.vhd /designs/avs_AES/subdesigns/mux3_IOwidth32_205
::legacy::set_attribute -quiet is_sop_cluster true /designs/avs_AES/subdesigns/aes_fsm_encrypt_NO_ROUNDS14
::legacy::set_attribute -quiet hdl_user_name aes_fsm_encrypt /designs/avs_AES/subdesigns/aes_fsm_encrypt_NO_ROUNDS14
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/aes_fsm_encrypt.vhd} {./src}}} /designs/avs_AES/subdesigns/aes_fsm_encrypt_NO_ROUNDS14
::legacy::set_attribute -quiet arch_filename ./src/aes_fsm_encrypt.vhd /designs/avs_AES/subdesigns/aes_fsm_encrypt_NO_ROUNDS14
::legacy::set_attribute -quiet entity_filename ./src/aes_fsm_encrypt.vhd /designs/avs_AES/subdesigns/aes_fsm_encrypt_NO_ROUNDS14
::legacy::set_attribute -quiet preserve true /designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/FSM_ENC/nets/clk
::legacy::set_attribute -quiet hdl_user_name AddRoundKey /designs/avs_AES/subdesigns/AddRoundKey
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/addroundkey.vhd} {./src}}} /designs/avs_AES/subdesigns/AddRoundKey
::legacy::set_attribute -quiet arch_filename ./src/addroundkey.vhd /designs/avs_AES/subdesigns/AddRoundKey
::legacy::set_attribute -quiet entity_filename ./src/addroundkey.vhd /designs/avs_AES/subdesigns/AddRoundKey
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_fwd
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_fwd.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_fwd
::legacy::set_attribute -quiet arch_filename ./src/mixcol_fwd.vhd /designs/avs_AES/subdesigns/Mixcol_fwd
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_fwd
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_fwd_171
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_fwd.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_fwd_171
::legacy::set_attribute -quiet arch_filename ./src/mixcol_fwd.vhd /designs/avs_AES/subdesigns/Mixcol_fwd_171
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_fwd_171
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_fwd_170
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_fwd.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_fwd_170
::legacy::set_attribute -quiet arch_filename ./src/mixcol_fwd.vhd /designs/avs_AES/subdesigns/Mixcol_fwd_170
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_fwd_170
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_fwd_169
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_fwd.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_fwd_169
::legacy::set_attribute -quiet arch_filename ./src/mixcol_fwd.vhd /designs/avs_AES/subdesigns/Mixcol_fwd_169
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_fwd_169
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_190
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_190
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_190
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_190
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_189
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_189
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_189
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_189
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_188
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_188
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_188
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_188
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_187
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_187
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_187
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_187
::legacy::set_attribute -quiet is_sop_cluster true /designs/avs_AES/subdesigns/aes_fsm_decrypt_NO_ROUNDS14
::legacy::set_attribute -quiet hdl_user_name aes_fsm_decrypt /designs/avs_AES/subdesigns/aes_fsm_decrypt_NO_ROUNDS14
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/aes_fsm_decrypt.vhd} {./src}}} /designs/avs_AES/subdesigns/aes_fsm_decrypt_NO_ROUNDS14
::legacy::set_attribute -quiet arch_filename ./src/aes_fsm_decrypt.vhd /designs/avs_AES/subdesigns/aes_fsm_decrypt_NO_ROUNDS14
::legacy::set_attribute -quiet entity_filename ./src/aes_fsm_decrypt.vhd /designs/avs_AES/subdesigns/aes_fsm_decrypt_NO_ROUNDS14
::legacy::set_attribute -quiet preserve true /designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.FSM_DEC/nets/clk
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_inv
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_inv.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_inv
::legacy::set_attribute -quiet arch_filename ./src/mixcol_inv.vhd /designs/avs_AES/subdesigns/Mixcol_inv
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_inv
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_inv_174
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_inv.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_inv_174
::legacy::set_attribute -quiet arch_filename ./src/mixcol_inv.vhd /designs/avs_AES/subdesigns/Mixcol_inv_174
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_inv_174
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_inv_173
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_inv.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_inv_173
::legacy::set_attribute -quiet arch_filename ./src/mixcol_inv.vhd /designs/avs_AES/subdesigns/Mixcol_inv_173
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_inv_173
::legacy::set_attribute -quiet hdl_user_name Mixcol /designs/avs_AES/subdesigns/Mixcol_inv_172
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mixcol.vhd src/mixcol_inv.vhd} {./src}}} /designs/avs_AES/subdesigns/Mixcol_inv_172
::legacy::set_attribute -quiet arch_filename ./src/mixcol_inv.vhd /designs/avs_AES/subdesigns/Mixcol_inv_172
::legacy::set_attribute -quiet entity_filename ./src/mixcol.vhd /designs/avs_AES/subdesigns/Mixcol_inv_172
::legacy::set_attribute -quiet hdl_user_name AddRoundKey /designs/avs_AES/subdesigns/AddRoundKey_168
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/addroundkey.vhd} {./src}}} /designs/avs_AES/subdesigns/AddRoundKey_168
::legacy::set_attribute -quiet arch_filename ./src/addroundkey.vhd /designs/avs_AES/subdesigns/AddRoundKey_168
::legacy::set_attribute -quiet entity_filename ./src/addroundkey.vhd /designs/avs_AES/subdesigns/AddRoundKey_168
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_186
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_186
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_186
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_186
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_185
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_185
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_185
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_185
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_184
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_184
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_184
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_184
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_183
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_183
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_183
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_183
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[0].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1_223
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1_223
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_223
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_223
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[0].LowWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1_222
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1_222
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_222
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_222
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[1].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1_221
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1_221
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_221
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_221
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[1].LowWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1_220
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1_220
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_220
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_220
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[2].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1_219
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1_219
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_219
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_219
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[2].LowWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1_218
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1_218
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_218
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_218
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[3].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE1_217
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE1_217
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_217
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE1_217
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/decrypt_datapath.sboxROMs_dec[3].LowWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_194
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_194
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_194
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_194
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_193
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_193
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_193
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_193
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_192
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_192
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_192
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_192
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_191
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_191
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_191
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_191
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth4
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth4
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth4
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth4
::legacy::set_attribute -quiet is_sop_cluster true /designs/avs_AES/subdesigns/keyexpansionV2_KEYLENGTH256
::legacy::set_attribute -quiet hdl_user_name keyexpansionV2 /designs/avs_AES/subdesigns/keyexpansionV2_KEYLENGTH256
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/mux2.vhd src/memory_word.vhd src/keyexpansionV2.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/keyexpansionV2_KEYLENGTH256
::legacy::set_attribute -quiet arch_filename ./src/keyexpansionV2.vhd /designs/avs_AES/subdesigns/keyexpansionV2_KEYLENGTH256
::legacy::set_attribute -quiet entity_filename ./src/keyexpansionV2.vhd /designs/avs_AES/subdesigns/keyexpansionV2_KEYLENGTH256
::legacy::set_attribute -quiet preserve true /designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/nets/clk
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0
::legacy::set_attribute -quiet preserve true /designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/HighWord/nets/clk
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_216
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_216
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_216
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_216
::legacy::set_attribute -quiet preserve true /designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/LowWord/nets/clk
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_196
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_196
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_196
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_196
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_197
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_197
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_197
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_197
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_195
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_195
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_195
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_195
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[0].rest_of_shiftreg.keywordregister/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32_182
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32_182
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_182
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_182
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[1].rest_of_shiftreg.keywordregister/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_204
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_204
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_204
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_204
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32_181
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32_181
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_181
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_181
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[2].rest_of_shiftreg.keywordregister/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_203
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_203
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_203
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_203
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32_180
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32_180
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_180
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_180
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[3].rest_of_shiftreg.keywordregister/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_202
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_202
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_202
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_202
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32_179
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32_179
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_179
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_179
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[4].rest_of_shiftreg.keywordregister/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_201
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_201
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_201
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_201
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32_178
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32_178
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_178
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_178
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[5].rest_of_shiftreg.keywordregister/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_200
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_200
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_200
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_200
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32_177
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32_177
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_177
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_177
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[6].rest_of_shiftreg.keywordregister/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_199
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_199
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_199
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_199
::legacy::set_attribute -quiet hdl_user_name memory_word /designs/avs_AES/subdesigns/memory_word_IOwidth32_176
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/memory_word.vhd} {./src}}} /designs/avs_AES/subdesigns/memory_word_IOwidth32_176
::legacy::set_attribute -quiet arch_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_176
::legacy::set_attribute -quiet entity_filename ./src/memory_word.vhd /designs/avs_AES/subdesigns/memory_word_IOwidth32_176
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/roundkey_generator/instances_hier/Shiftreg[7].lastDWORD.last_keywordreg/nets/clk}
::legacy::set_attribute -quiet hdl_user_name mux2 /designs/avs_AES/subdesigns/mux2_IOwidth32_198
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/mux2.vhd} {./src}}} /designs/avs_AES/subdesigns/mux2_IOwidth32_198
::legacy::set_attribute -quiet arch_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_198
::legacy::set_attribute -quiet entity_filename ./src/mux2.vhd /designs/avs_AES/subdesigns/mux2_IOwidth32_198
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_215
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_215
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_215
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_215
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[0].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_214
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_214
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_214
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_214
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[0].LowWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_213
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_213
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_213
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_213
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[1].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_212
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_212
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_212
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_212
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[1].LowWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_211
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_211
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_211
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_211
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[2].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_210
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_210
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_210
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_210
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[2].LowWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_209
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_209
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_209
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_209
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[3].HighWord/nets/clk}
::legacy::set_attribute -quiet hdl_user_name sbox /designs/avs_AES/subdesigns/sbox_INVERSE0_208
::legacy::set_attribute -quiet hdl_filelist {{default -vhdl1993 {SYNTHESIS} {src/avs_aes_pkg.vhd src/sbox.vhd src/sboxM4k.vhd} {./src}}} /designs/avs_AES/subdesigns/sbox_INVERSE0_208
::legacy::set_attribute -quiet arch_filename ./src/sboxM4k.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_208
::legacy::set_attribute -quiet entity_filename ./src/sbox.vhd /designs/avs_AES/subdesigns/sbox_INVERSE0_208
::legacy::set_attribute -quiet preserve true {/designs/avs_AES/instances_hier/AES_CORE_1/instances_hier/sboxROMs_enc[3].LowWord/nets/clk}
