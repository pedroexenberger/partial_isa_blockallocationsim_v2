## Template para síntese lógica no Cadence GENUS
## Autor: Marcelo Brandalero
##
##
## Data: 14.08.2018
##
## Modificado por: Pedro
##
## Data: 22.11.2018
##
## Modificado por: Pedro
##
## Data: 07.03.2019
## 
## Modificações: modificado para A15 like

set DESIGN_NAME [basename [lpwd]]

# Seu design aqui - arquivos que serão lidos
set DESIGNS "src/boom.system.TestHarness.${DESIGN_NAME}.v"
# Seu modulo top aqui
set MODULE_NAME "BoomTile"
# Isso aqui é pra fazer análise de potencia - o vcd é o waveform que é carregado antes da síntese pra estimar o chaveamento e potencia das coisas, e consequentemente ele ir otimizando durante a sintese. tem que tirar fora se não tiver vcd
#set VCD_PATH "/workareas/marcelo.brandalero/Rocket.vcd"

# Pedro: no meu caso uso o saif, que é menor.
# Não uso o saif nesse caso, só quero área para o trabalho do partial isa
#set SAIF_PATH "saif/${DESIGN_NAME}.saif"

#clock delay
#set CLOCK_DELAY 2000
set CLOCK_DELAY 500

# auto-explicativo
# o map-effort tem que ser no maximo medium se tu não quiser que ele desagrupe os modulos. se colocar em high tu tens um design otimizado, mas não vais conseguir fazer o breakdown da potencia/area das coisas


set SYN_EFF medium
set MAP_EFF medium

# caminho da lib
set_attribute lib_search_path /home/gme/pedro.becker/NanGate_15nm_OCL_v0.1_2014_06.A/front_end/timing_power_noise/CCS/
# nome do arquivo da lib (dentro do caminho)
set_attribute library NanGate_15nm_OCL_typical_conditional_ccs.lib

# serviria pra usar o mesmo modelo de fio (capacitancias) pra todo o circuito, mas a biblioteca que eu uso não tem isso
# se a biblioteca que tu usar tiver, tem que setar o atributo wireload_MODEL (nota que é outro) pro nome do modelo
set_attribute wireload_mode top
# lp são os atributos relacionados à analise de potencia
# default_probability é a probabilidade de uma rede estar em nível lógico 1
# tem que setar também o lp_default_toggle_rate, que é dado em chaveamentos/ns (ou ps, dependendo da lib) - aqui eu não setei pq a análise era feita em cima do VCD
set_attribute lp_default_probability 0.5
# auto explicativo
set_attribute lp_power_analysis_effort high

# aqui o toggle rate que falei acima
# a conta se refere a "metade do chavemento maximo" - nota que ele chaveia 0.5 vezes a cada ciclo de clock
set_attribute lp_default_toggle_rate [expr 0.5 * double(1000) / ${CLOCK_DELAY}]

# auto explicativo
read_hdl $DESIGNS
puts "Runtime & Memory after 'read_hdl'"
timestat

# transforma os hdl em uma representação na memoria
elaborate $MODULE_NAME
puts "Runtime & Memory after 'elaborate'"
timestat

# le o vcd - se não tiver, comenta fora
#read_vcd -scale [expr double(${CLOCK_DELAY}) / 2000] -static -ignorecase -vcd_scope core -start_time 400000000 -end_time 450000000 ${VCD_PATH}

# le o saif
#read_saif -scale [expr double(${CLOCK_DELAY}) / 2000] -instance BoomCore $SAIF_PATH

# pra otimizar pra potencia dinamica, seta max potencia dinamica = 0
set_attribute max_dynamic_power 0 [find / -des *]

# isso aqui é pra ele tratar as redes de clock como ideais, sem delay
# a geração da árvore de clock, que minimiza os delays dessa rede, é feita apenas durante o place & route, que é posterior à sintese logica que é feita aqui
create_clock -period ${CLOCK_DELAY} clock
set_ideal_network clock
set_dont_touch_network clock

# síntese "to generic" - quer dizer, gera netlist independente da tecnologia
synthesize -to_generic -eff ${SYN_EFF} ${MODULE_NAME}
puts "Runtime & Memory after 'synthesize -to_generic'"
timestat

# mapeia o netlist gerado para a biblioteca de células
synthesize -to_mapped  -eff ${MAP_EFF} ${MODULE_NAME}
puts "Runtime & Memory after 'synthesize -to_mapped'"
timestat

# auto explicativo - dá pra passar as opções "-detail" aqui
report timing  > reports/timing/timing_${DESIGN_NAME}_default.txt
report area    > reports/area/area_${DESIGN_NAME}_default.txt
report power   > reports/power/power_${DESIGN_NAME}_default.txt

# auto explicativo - o -tcf gera um arquivo com atividade de chaveamento, se tu for usar um vcd ou coisa parecida
write_design -basename designs/${DESIGN_NAME} -hierarchical -tcf

quit
