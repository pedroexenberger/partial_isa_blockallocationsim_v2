# using as enum
class BlockType:
    INT = "int"
    FP = "fp"
    VIDEO = "video"
    NN = "nn"
    DECRYPT = "decrypt"
    ENCRYPT = "encrypt"
    NONE = None

    __accelerated = [VIDEO, NN, DECRYPT, ENCRYPT]
    
    @staticmethod
    def from_file(data_from_file):
        if data_from_file == 'i':
            return BlockType.INT
        elif data_from_file == 'f':
            return BlockType.FP
        elif data_from_file == 'v':
            return BlockType.VIDEO
        elif data_from_file == 'n':
            return BlockType.NN
        elif data_from_file == 'd':
            return BlockType.DECRYPT
        elif data_from_file == 'e':
            return BlockType.ENCRYPT
        else:
            raise ValueError("Unknown BlockType from file <%s>" %
                data_from_file)

    @staticmethod
    def is_accelerated(block_type):
        return block_type in BlockType.__accelerated


class ReadBlockStatus:
    OK              = 0
    FINISH          = -1
    NO_END          = -2
    INVALID_START   = -3
    READ_ACC        = -4
    READ_END_ACC    = -5


class Block(object):
    def __init__(self):
        self.block_start = 0
        self.block_end = 0
        self.block_id = 0
        self.last_block_end = 0
        self.block_type = BlockType.NONE
        self.file_ptr_previous_block = 0

    def read_block_from(self, file):
        if not file.closed:
            self.file_ptr_previous_block = file.tell()
            block_start_line = file.readline()
            if block_start_line.strip() != '':

                block_tick, block_status, block_type, block_id = self.parse_block_line(block_start_line)

                if block_status == 's':
                    self.block_start = block_tick
                    self.block_type = block_type
                    self.block_id = block_id
                    if BlockType.is_accelerated(self.block_type):
                        self.block_end = 0
                        return ReadBlockStatus.READ_ACC
                    block_finish_line = file.readline()
                    if block_finish_line.strip() != '':

                        block_tick, block_status, block_type, block_id = self.parse_block_line(block_finish_line)

                        if block_status == 'f':
                            self.block_end = block_tick
                            self.block_type = block_type
                            self.block_id = block_id
                            #print("reading block {} {}".format(block_type, block_id))
                            return ReadBlockStatus.OK
                        else:
                            return ReadBlockStatus.NO_END
                    else:
                        return ReadBlockStatus.NO_END
                elif block_status == 'f':
                    if BlockType.is_accelerated(block_type):
                        return ReadBlockStatus.READ_END_ACC
                else:
                    return ReadBlockStatus.INVALID_START
            else:
                file.close()
                return ReadBlockStatus.FINISH
        else:
            return ReadBlockStatus.FINISH

    def find_current_block_end(self, file):
        if not file.closed:
            while True:
                block_finish_line = file.readline()
                if block_finish_line.strip() != '':

                    block_tick, block_status, block_type, block_id = \
                        self.parse_block_line(block_finish_line)

                    # Loop until you find the end of the current block type
                    if (block_type == self.block_type) and block_status == 'f':
                        self.block_end = block_tick
                        self.block_type = block_type
                        self.block_id = block_id
                        return ReadBlockStatus.OK
                    elif (block_type == self.block_type) and block_status == 's':
                        return ReadBlockStatus.NO_END
                    else:
                        continue
                else:
                    file.close()
                    return ReadBlockStatus.NO_END
        else:
            return ReadBlockStatus.NO_END

    def parse_block_line(self, line):
        # Split undesired trailing information from the line
        dataset = line.split(":")
        # Useful information is in index 2 with format:
        # tick;s(start)/f(finish);type;id
        dataset = dataset[2].split(";")

        block_tick = int(dataset[0])
        block_status = dataset[1]
        block_type = BlockType.from_file(dataset[2])
        block_id = int(dataset[3])

        return block_tick, block_status, block_type, block_id

    def rewind_block(self, file):
        file.seek(self.file_ptr_previous_block)
