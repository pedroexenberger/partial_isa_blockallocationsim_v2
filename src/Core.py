import Exceptions
from Workload import Workload
from Block import ReadBlockStatus
import Stats #cannot use from because of cyclic dependency

EMULATION_COST = 40

# using as enum
class CoreType:
    BIG = "big"
    LITTLE = "little"
    VIDEO = "video"
    NN = "nn"
    DECRYPT = "decrypt"
    ENCRYPT = "encrypt"
    NONE = None

class AcceleratorSpeedup:
    VIDEO   = 75 
    NN      = 100
    DECRYPT = 25  
    ENCRYPT = 25

class MigrationCost:
    BIG     = 12000*313
    LITTLE  = 17000*313
    VIDEO   = 17000*313
    NN      = 17000*313
    AES     = 17000*313

class PowerCost:
    PWR_LITTLE_FULL = 0.08551675672     #in W
    PWR_LITTLE_PARTIAL = 0.032008779    #in W
    PWR_BIG_FULL = 0.7669147314         #in W
    PWR_BIG_PARTIAL = 0.35785042        #in W
    PWR_VIDEO = 0.02907273596           #in W
    PWR_NN = 0.123938031                #in W
    PWR_AES = 0.004421937528            #in W 

class Core(object):
    def __init__(self, core_id, core_description):
        # Internal ticks count
        self.cur_tick = 0
        self.exec_ticks = 0
        self.overhead_ticks = 0
        self.active_ticks = 0
        self.emulated_ticks = 0
        self.idle_ticks = 0
        self.cur_phase_tick = 0

        # Core data/characteristics
        self.core_id                    = core_id
        self.workload                   = Workload()
        self.supported_isa_extensions   = self.parse_isa(core_description)
        self.core_type                  = core_description["processor"]
        self.can_emulate                = core_description["canEmulate"]
        self.is_accelerator             = core_description["isAcc"]
        #self.accelerator_type           = core_description["accType"] #todo verificar, talvez inutil

        # Core stats
        # self.usage_hist			    = collections.OrderedDict()
        # self.usage_hist[0]            = core_id*2+1
        # self.migration_hist		    = {}
        self.stats = Stats.Stats()

    def get_power_cost(self):
        if self.core_type == CoreType.BIG:
            if "fp" in self.supported_isa_extensions:
                return PowerCost.PWR_BIG_FULL
            else:
                return PowerCost.PWR_BIG_PARTIAL
        elif self.core_type == CoreType.LITTLE:
            if "fp" in self.supported_isa_extensions:
                return PowerCost.PWR_LITTLE_FULL
            else:
                return PowerCost.PWR_LITTLE_PARTIAL
        elif self.core_type == CoreType.VIDEO:
            if "video" in self.supported_isa_extensions:
                return PowerCost.PWR_VIDEO
            else:
                raise AttributeError("Unknown extension on Video Accelerator")
        elif self.core_type == CoreType.NN:
            if "nn" in self.supported_isa_extensions:
                return PowerCost.PWR_NN
            else:
                raise AttributeError("Unknown extension on NN Accelerator")
        elif self.core_type == CoreType.DECRYPT:
            if "decrypt" in self.supported_isa_extensions:
                return PowerCost.PWR_AES
            else:
                raise AttributeError("Unknown extension on Decrypt Accelerator")
        elif self.core_type == CoreType.ENCRYPT:
            if "encrypt" in self.supported_isa_extensions:
                return PowerCost.PWR_AES
            else:
                raise AttributeError("Unknown extension on Encrypt Accelerator")
        else:
            raise AttributeError("Unknown CoreType")

    def parse_isa(self, core_description):
        isa_extensions = []
        for extension in str(core_description["isaExtension"]).split(","):
            isa_extensions.append(extension)
        return isa_extensions

    def set_workload(self, workload):
        assert (self.workload.pid is None or (self.workload.pid == workload.pid)),\
                "Assigning new workload to active core"
        if self.workload.pid is None and workload.pid is not None:
            self.add_migration_cost()
        self.workload = workload
        self.cur_phase_tick = 0
        self.stat_start_execution_interval()

    def get_core_desc(self):
        core_desc = self.core_type
        if self.is_accelerator:
            core_desc = core_desc + " acc"
        else:
            if "fp" in self.supported_isa_extensions:
                core_desc = core_desc + " full"
            else:
                core_desc = core_desc + " partial"
        return core_desc

    def stat_start_execution_interval(self):
        if self.workload.previous_holding_core_id != self.core_id:
            depends_on_isa_extension = self.workload.depends_on_isa_extension is not None
            self.stats.add_migration(self.core_id, self.workload.pid, self.cur_tick, depends_on_isa_extension)
        self.stats.set_start_execution_interval(self.core_id, self.workload.pid, self.get_core_desc(), self.workload.benchmark, self.cur_tick/313)

    def stat_end_execution_interval(self):
        self.workload.previous_holding_core_id = self.core_id # set the current core as the previous, so we track core migrations
        self.stats.set_end_execution_interval(self.core_id, self.workload.pid, self.cur_tick/313)

    def add_migration_cost(self):
        if self.core_type == CoreType.BIG:
            overhead_ticks = MigrationCost.BIG
        elif self.core_type == CoreType.LITTLE:
            overhead_ticks = MigrationCost.LITTLE
        elif self.core_type == CoreType.VIDEO:
            overhead_ticks = MigrationCost.VIDEO
        elif self.core_type == CoreType.NN:
            overhead_ticks = MigrationCost.NN
        elif self.core_type == CoreType.DECRYPT:
            overhead_ticks = MigrationCost.AES
        elif self.core_type == CoreType.ENCRYPT:
            overhead_ticks = MigrationCost.AES
        else:
            ValueError("Unknown core type to add migration cost")
        self.overhead_ticks += overhead_ticks
        self.cur_tick += overhead_ticks

    def process_workload(self):
        #print("processing workload {} at core: {}".format(self.workload.benchmark, self.core_id))
        self.execute_block_from_workload()
        self.workload.cur_tick = self.cur_tick
        self.workload.prefer_core_support = self.workload.curr_block_type()

    def execute_block_from_workload(self):
        if self.is_accelerator and self.workload.waiting_for_acc:
            read_block_status = self.workload.read_accelerated_block_end()
        else:
            read_block_status = self.workload.read_block()
        if read_block_status == ReadBlockStatus.OK or read_block_status == ReadBlockStatus.READ_END_ACC:
            if not self.can_execute_workload_block():
                self.workload.set_isa_extension_depency()
                return
                # raise CoreCannotExecuteBlockError
            block_ticks = self.workload_block_ticks_for_core_type()
            need_emulation = self.workload_need_emulation()
            if need_emulation:
                block_ticks = block_ticks * EMULATION_COST
                self.emulated_ticks += block_ticks
            self.active_ticks += block_ticks
            self.exec_ticks += block_ticks
            self.cur_tick += block_ticks
            self.cur_phase_tick += block_ticks
        if read_block_status == ReadBlockStatus.FINISH:
            self.remove_current_workload_from_core()
        if read_block_status == ReadBlockStatus.READ_ACC:
            self.workload.set_isa_extension_depency()
        

    def can_execute_workload_block(self):
        block_type = self.workload.curr_block_type()
        if block_type in self.supported_isa_extensions or self.can_emulate:
            return True
        else:
            return False

    def workload_need_emulation(self):
        if self.workload.curr_block_type() in self.supported_isa_extensions:
            return False
        else:
            return True

    def workload_block_ticks_for_core_type(self):
        if self.core_type == CoreType.BIG:
            return self.__workload_delta_ticks_big_block()

        elif self.core_type == CoreType.LITTLE:
            return self.__workload_delta_ticks_little_block()

        elif self.core_type == CoreType.VIDEO:
            return self.__workload_delta_ticks_big_block()/AcceleratorSpeedup.VIDEO

        elif self.core_type == CoreType.NN:
            return self.__workload_delta_ticks_big_block()/AcceleratorSpeedup.NN

        elif self.core_type == CoreType.DECRYPT:
            return self.__workload_delta_ticks_big_block()/AcceleratorSpeedup.DECRYPT

        elif self.core_type == CoreType.ENCRYPT:
            return self.__workload_delta_ticks_big_block()/AcceleratorSpeedup.ENCRYPT

        else:
            raise ValueError('CoreType <%s> of core %s is an invalid core type' % (self.core_type, self.core_id))

    def is_gpp(self):
        return not self.is_accelerator

    def has_workload(self):
        return self.workload.pid is not None

    def remove_current_workload_from_core(self):
        self.stat_end_execution_interval()
        self.workload = Workload()
        self.cur_phase_tick = 0

    def __workload_delta_ticks_big_block(self):
        # private method starts with __
        return self.workload.big_block.block_end - self.workload.big_block.block_start

    def __workload_delta_ticks_little_block(self):
        # private method starts with __
        return self.workload.little_block.block_end - self.workload.little_block.block_start
