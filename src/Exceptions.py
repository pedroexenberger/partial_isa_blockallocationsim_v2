class Error(Exception):
    """Base class for other exceptions"""
    pass

class CoreCannotExecuteBlockError(Error):
    """Raised when core cannot execute a given block type"""
    pass
