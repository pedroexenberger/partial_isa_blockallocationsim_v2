from WorkloadQueue import WorkloadQueue
from Workload import Workload
from Core import Core
from Core import CoreType
from Stats import Stats

class SchedulerMode:
    Performance = "performance"
    Energy      = "energy"

class SchedulerThresholds:
    PhaseTick   = 160000*313

class Scheduler(object):
    def __init__(self, mode, cores_description, workloads_description):
        self.workloads_queue = WorkloadQueue(workloads_description)
        self.cores = self.create_cores_list(cores_description)
        self.mode = mode
        self.stats = Stats()
        self.stats.inform_system_cores(cores=self.cores)

    def pick_workload_for_core(self, core):
        "Selects an appropriate workload to execute on a core"
        chosen_workload = Workload()
        self.workloads_queue.reorder_workloads_by_priority()
        for workload in self.workloads_queue.queue:
            if self.workload_ready_at_tick(workload, core.cur_tick):
                # workload was not supported by previous core holder
                if workload.depends_on_isa_extension is not None:
                    if workload.depends_on_isa_extension in core.supported_isa_extensions:
                        chosen_workload = workload
                        break
                    else:
                        continue
                else:
                    #prefer tasks it will not immediately evict
                    if workload.prefer_core_support is None:
                        chosen_workload = workload
                        break
                    elif workload.prefer_core_support in core.supported_isa_extensions:
                        chosen_workload = workload
                        break
                    else:
                        continue
        if chosen_workload.pid is not None:
            self.workloads_queue.remove_workload_from_queue(chosen_workload)
            chosen_workload.remove_isa_extension_dependency()
        return chosen_workload
            #...
        # assert workload.curtick > core.curtick
        # 
        # chosenWorkload.remove_isa_extension_dependency()
        # return chosenWorkload


    def workload_ready_at_tick(self, workload, tick):
        "checks if workload is ready at the tick moment"
        if tick >= workload.cur_tick:
            return True
        else:
            return False

    def enqueue_workload(self, workload):
        "Add a workload in the task queue"
        self.workloads_queue.append_workload_in_queue(workload)

    def insert_workload_at_queue_head(self, workload):
        "Insert a workload in the head of the queue"
        self.workloads_queue.insert_workload_in_queue_head(workload)

    def evaluate_migration(self, core, workload):
        "Defines whether a task should be evicted from a core"
        # if workload depends on an isa_extension its because it could not execute in the current core
            # recall to remove isa dependency every_time allocates it to a new core (hopefully, scheduler will know what it is doing) see pick_workload_for core
            # recall to rewind the file if a block status is unsuccessful and someone else can execute it
            # if anyone else can execute it (e.g., no accelerator available, ignore the block and realloc to a core)
        # if the workload has been in the core for too long
        if workload.depends_on_isa_extension is not None:
            if not workload.waiting_for_acc:
                # depending on isa extension, not accelerator. rewind block
                workload.rewind_workload_to_previous_block()
            elif not self.has_accelerator_for_block(workload):
                # system does not have an accelerator to execute this workload block
                # ignore the dependency and execute block through CPU
                workload.remove_isa_extension_dependency()
            core.remove_current_workload_from_core()
            self.insert_workload_at_queue_head(workload)
        elif core.cur_phase_tick >= SchedulerThresholds.PhaseTick:
            core.remove_current_workload_from_core()
            self.enqueue_workload(workload)

    def keep_execution(self):
        "check workloads queue and finish simulation when it is empty"
        if self.workloads_queue.is_empty():
            for core in self.cores:
                if core.workload.pid is not None:
                    return True
            return False
        else:
            return True

    def create_cores_list(self, cores_description):
        cores = []
        for key, core_desc in enumerate(cores_description):
            core = Core(core_id=key,
                        core_description=core_desc["core"])
            cores.append(core)
        return cores

    def pick_core(self, at_least_ticks):
        target_core = None
        for core in self.cores:
            if core.cur_tick >= at_least_ticks:
                if target_core is None:
                    target_core = core
                if core.cur_tick < target_core.cur_tick:
                    target_core = core
                elif core.cur_tick == target_core.cur_tick:
                    if self.mode == SchedulerMode.Performance:
                        #swap if big available and currently in a little
                        if target_core.core_type == CoreType.LITTLE and core.core_type == CoreType.BIG:
                            target_core = core
                        else:
                            #if both are same time, prefer the simpler first
                            if target_core.core_type == core.core_type:
                                if len(core.supported_isa_extensions) < len(target_core.supported_isa_extensions):
                                    target_core = core    
                    elif self.mode == SchedulerMode.Energy:
                        #swap if little available and currently in a big
                        if target_core.core_type == CoreType.BIG and core.core_type == CoreType.LITTLE:
                            target_core = core
                        else:
                            #if both are same time, prefer the simpler first
                            if target_core.core_type == core.core_type:
                                if len(core.supported_isa_extensions) < len(target_core.supported_isa_extensions):
                                    target_core = core
                    # When more than one core has same cur_tick, chose accordingly with scheduler policy
        return target_core

    def has_accelerator_for_block(self, workload):
        for core in self.cores:
            if workload.depends_on_isa_extension in core.supported_isa_extensions:
                return True
        return False