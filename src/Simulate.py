#!/usr/bin/python3

from SystemManager import SystemManager
import sys
import json
import ptvsd
from pathlib import Path
from Stats import Stats

def main():
    if len(sys.argv) < 3:
        print("Usage: Simulate.py <configuration> <scenario> [<scheduler_mode 'performance' or 'energy'>]")
        sys.exit(1)
    # vs code debug block 
    #5678 is the default attach port in the VS Code debug configurations
    # sys.stderr.write("Waiting for debugger attach")
    # sys.stderr.flush()
    # ptvsd.enable_attach(address=('localhost', 5678), redirect_output=True)
    # ptvsd.wait_for_attach()
    # breakpoint()
    #end vs code debug block
    configuration = Path(sys.argv[1]).stem
    scenario = Path(sys.argv[2]).stem
    with open(sys.argv[1],"r") as f:
        cores_desc = json.load(f)["cores"]
    with open(sys.argv[2],"r") as f:
        workloads_desc = json.load(f)["workloads"]
    
    if len(sys.argv) == 4: 
        scheduler_mode = sys.argv[3]
        if scheduler_mode != "performance" and scheduler_mode != "energy":
            print("Usage: Simulate.py <configuration> <scenario> [<scheduler_mode 'performance' or 'energy'>]")
            sys.exit(1)
        system_manager = SystemManager(configuration, scenario, cores_desc, workloads_desc, scheduler_mode=scheduler_mode)
    else:
        system_manager = SystemManager(configuration, scenario, cores_desc, workloads_desc)
    
    system_manager.run()
    system_manager.print_final_stats()
    system_manager.save_final_stats()
    system_manager.plot_executions()
    #system_manager.plot_edp()
    

if __name__ == "__main__":	
	main()