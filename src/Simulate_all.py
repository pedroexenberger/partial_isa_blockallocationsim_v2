#!/usr/bin/python3
#!/usr/local/bin/python3

from SystemManager import SystemManager
import sys
import os
import json
import time
from pathlib import Path
from Stats import Stats

def main():
    # scenarios = os.listdir("../scenarios/")
    # scenarios.sort()
    # for scenario in scenarios:
    scenario_file = sys.argv[1]
    if scenario_file.endswith(".json"):
#        scenario_file = "../scenarios/" + scenario
        with open(scenario_file,"r") as f:
            workloads_desc = json.load(f)["workloads"]
        scenario = Path(scenario_file).stem
        configurations = os.listdir("../configurations/")
        configurations.sort()
        for configuration in configurations:
            if configuration.endswith(".json"):
                print("Running scenario: {0} configuration: {1}".format(scenario,configuration))
                
                configuration_file = "../configurations/" + configuration
                with open(configuration_file,"r") as f:
                    cores_desc = json.load(f)["cores"]
                configuration = Path(configuration).stem
                system_manager = SystemManager(configuration, scenario, cores_desc, workloads_desc)
                system_manager.run()
                system_manager.save_final_stats()
                #system_manager.plot_executions()
        #system_manager.plot_edp()

if __name__ == "__main__":
    main()