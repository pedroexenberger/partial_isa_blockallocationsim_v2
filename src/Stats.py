import sys
import os
import ptvsd
import plotly as py
import plotly.figure_factory as ff
import plotly.express as px
import plotly.graph_objs as go
import pandas as pd
import numpy as np
from random import shuffle
from scipy.stats.mstats import gmean
from plotly.subplots import make_subplots
import Core

BASE_OUTPUT_PATH = "../output/"
STATS_FILE= "stats.pkl"

class Migration():
    core_id                 = None
    core_desc               = None
    pid                     = None
    benchmark               = None
    timestamp               = None
    needed_isa_extension    = None
    looked_for_accelerator  = None

class ExecutionInterval():
    core_id     = None
    pid         = None
    start_time  = None
    end_time    = None

class UtilConstants():
    TicksPerCycle = 313
    ClockCycleBig = 3.2 * 1000000000 #3.2GHz clock do big no gem5

class UtilName():
    scenarios_dict = {
                    'edge_computing':'Edge Computing',
                    'fp_driven':'FP Driven',
                    'multitask':'Multitask',
                    'road_sign_detection':'Road Sign Detection',
                    'smartphone_app':'Smartphone App',
                    'video_streaming':'Video Streaming',
                    'geomean':'Geomean'}
    configurations_dict = {
                    'accelerator_rich' : 'Accelerator Rich',
                    'aes_accelerated' : 'AES Accelerated',
                    'baseline':'Baseline',
                    'nn_accelerated':'CNN Accelerated',
                    'task_parallel':'Task Parallel',
                    'video_accelerated':'Video Accelerated',
                    }

    @staticmethod 
    def get_scenarios_label(key_list):
        return [UtilName.scenarios_dict[k] for k in key_list]

    @staticmethod 
    def get_scenario_label(key):
        return UtilName.scenarios_dict[key]

    @staticmethod 
    def get_configurations_label(key_list):
        return [UtilName.configurations_dict[k] for k in key_list]
    
    @staticmethod 
    def get_configuration_label(key):
        return UtilName.configurations_dict[key]

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

#Python3 - https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
class Stats(metaclass=Singleton):
    configuration       = None
    scenario            = None
    scheduler_mode      = None
    migrations          = []
    execution_intervals = []
    df_executions = []


    def inform_system_cores(self, cores=None):
        if cores is not None:
            for core in cores:
                core_description = "{0} - {1}".format(str(core.core_id).zfill(2), core.get_core_desc()) #todo add toString method in Core.py
                self.df_executions.append(
                    dict(Task = core_description,
                        Start = 0,
                        Finish = 0,
                        Resource=None)
                )

    def set_start_execution_interval(self, core_id, pid, core_desc, benchmark, start_time):
        execution_interval = ExecutionInterval()
        execution_interval.core_id = str(core_id).zfill(2)
        execution_interval.pid = pid
        execution_interval.core_desc = core_desc
        execution_interval.benchmark = benchmark
        execution_interval.start_time = start_time
        self.execution_intervals.append(execution_interval)

    def set_end_execution_interval(self, core_id, pid, end_time):
        target_execution_interval = next(ei for ei in reversed(self.execution_intervals) if (ei.core_id == str(core_id).zfill(2) and ei.pid == pid))
        target_execution_interval.end_time = end_time

    def add_migration(self, core_id, pid, timestamp, needed_isa_extension=False, looked_for_accelerator=False):
        migration = Migration()
        migration.core_id = str(core_id).zfill(2)
        migration.pid = pid
        migration.timestamp = timestamp
        migration.needed_isa_extension = needed_isa_extension
        migration.looked_for_accelerator = looked_for_accelerator
        self.migrations.append(migration)

    def get_migrations(self):
        return len(self.migrations)
    
    def get_isa_dependency_forced_migrations(self):
        return len([migration for migration in self.migrations if migration.needed_isa_extension == True])

    def get_looking_for_accelerator_migrations(self):
        return len([migration for migration in self.migrations if migration.looked_for_accelerator == True])

    def get_final_stats(self, cores):
        total_ticks = cores[0].cur_tick
        total_cycles = total_ticks / UtilConstants.TicksPerCycle
        total_time = total_cycles / UtilConstants.ClockCycleBig #little cycle is normalized in the gem5 config through higher latencies
        total_energy  = 0
        for core in cores:
            total_energy += core.active_ticks * core.get_power_cost()
        total_energy = total_energy / UtilConstants.TicksPerCycle / UtilConstants.ClockCycleBig
        total_migrations = self.get_migrations()
        return int(total_cycles), total_time, total_energy, total_migrations

    def print_stats(self, cores):
        total_cycles, total_time, total_energy, total_migrations = self.get_final_stats(cores)
        print("--------  Simulation End  --------")
        print("Scenario:\t\t {}".format(self.scenario))
        print("Configuration:\t\t {}".format(self.configuration))
        print("Total migrations:\t {}".format(total_migrations))
        print("Total cycles:\t\t {} s".format(total_cycles))    
        print("Total time:\t\t {} s".format(total_time))
        print("Total energy:\t\t {} J".format(total_energy))

    def save_stats_dataframe(self, cores):
        total_cycles, total_time, total_energy, total_migrations = self.get_final_stats(cores)
        sim_stats = {'Scenario': self.scenario,
                    'Configuration': self.configuration,
                    'Scheduler_mode': self.scheduler_mode,
                    'Total_cycles': total_cycles,
                    'Total_time': total_time,
                    'Total_energy': total_energy,    
                    'Total_migrations': total_migrations
        }
        if(os.path.exists(STATS_FILE)):
            df = pd.read_pickle(STATS_FILE)
        else:
            df = pd.DataFrame(columns=['Scenario', 'Configuration', 'Scheduler_mode' , 'Total_cycles', 'Total_time', 'Total_energy', 'Total_migrations'])
        df = df.append(sim_stats, ignore_index=True)
        df.drop_duplicates(subset=['Scenario', 'Configuration', 'Scheduler_mode'], keep='last', inplace = True)
        df = df.reset_index(drop=True)
        df.to_pickle(STATS_FILE)
        print(df)
        self.write_csv_df(BASE_OUTPUT_PATH, "df", df)


    def write_csv_df(self, path, filename, df):
        pathfile = os.path.normpath(os.path.join(path,filename))
        if (os.path.exists(pathfile)):
            os.remove(pathfile)
        df.to_csv("{0}.csv".format(pathfile), encoding='utf-8')

    def read_stats_dataframe(self, stats_file=STATS_FILE):
        if (os.path.exists(stats_file)):
            df = pd.read_pickle(stats_file)
            ordered_df = df
            #ordered_df = df.sort_values(['Scheduler_mode', 'Scenario', 'Configuration'])
        else:
            raise Exception("Unable to open stats log")
        return ordered_df

    def get_baseline_cycles_dataframe(self, df):
        baseline_row = df.query('Scenario == "{0}"\
                                & Configuration == "baseline"\
                                & Scheduler_mode == "{1}"'.format(self.scenario, self.scheduler_mode))
        return float(baseline_row['Total_cycles'])

    def add_all_cores_to_plot(self):
        # push empty execution intervals fore all cores
        # so all of them appear in the chart
        # uses whatever existing benchmark for the description because obligatory
        benchmark_description = "{0}".format(self.execution_intervals[0].benchmark)
        for execution in self.df_executions:
            execution.update({"Resource": benchmark_description})

    def add_all_execution_intervals_to_plot(self):
        # push all execution intervals into the plot
        for execution_interval in self.execution_intervals:
            core_description = "{0} - {1}".format(execution_interval.core_id, execution_interval.core_desc)
            benchmark_description = "{0}".format(execution_interval.benchmark)
            self.df_executions.append(
                dict(Task = core_description,
                    Start = execution_interval.start_time,
                    Finish = execution_interval.end_time,
                    Resource=benchmark_description)
            )
        self.df_executions = sorted(self.df_executions, 
                key = lambda elem: (elem['Task'], elem['Resource'])) #ordena por cores por padrao

    def get_color_dict(self):
        color_dict = dict()
        benchmarks_descriptions = list(set([dic['Resource'] for dic in self.df_executions])) #set used to get unique values only
        benchmarks_descriptions.sort()
        colorscale = px.colors.qualitative.Prism + px.colors.qualitative.Antique + px.colors.qualitative.Vivid
        for i, benchmark_description in enumerate(benchmarks_descriptions):
            color_dict[benchmark_description] = colorscale[i]
        return color_dict

    def adjust_chart_layout(self, layout):
        layout.title = None 
        layout.font = dict(family='Times New Roman', color='black', size=16)
        #type to use gantt with integer x axis
        max_x = self.get_baseline_cycles_dataframe(self.read_stats_dataframe())
        max_x += max_x/10
        layout.xaxis.update(type='-', title="Cycles", automargin=True, categoryorder="category ascending", range=[0, max_x])
        layout.yaxis.update(showgrid=True, autorange=True)
        layout.legend.traceorder="reversed"
        return layout

    def plot_executions(self):
        self.add_all_cores_to_plot()
        self.add_all_execution_intervals_to_plot()
        color_dict = self.get_color_dict()
        fig = ff.create_gantt(self.df_executions, colors=color_dict, index_col='Resource', show_colorbar=True, 
            group_tasks=True, show_hover_fill=False)
        fig.layout = self.adjust_chart_layout(fig.layout)
        fig.update_layout(
                autosize=False,
                height = 450,
                margin=go.layout.Margin(
                    l=40,
                    r=0,
                    b=0,
                    t=10
                )
        )
        
        if not os.path.exists(BASE_OUTPUT_PATH):
            os.mkdir(BASE_OUTPUT_PATH)
        #fig.write_image("{0}{1}-{2}-{3}.pdf".format(BASE_OUTPUT_PATH, self.scenario, self.configuration, self.scheduler_mode))
        #fig.show()     
        #fig.write_html("{0}{1}-{2}-{3}.html".format(BASE_OUTPUT_PATH, self.scenario, self.configuration, self.scheduler_mode))  
        fig.write_image("{0}{1}-{2}-{3}.pdf".format(BASE_OUTPUT_PATH, self.scenario, self.configuration, self.scheduler_mode))  

    def plot_edp(self):
        df = self.read_stats_dataframe()
        #plot baseline edp-iso curve
        baseline_row = df.query('Scenario == "{0}"\
                                & Configuration == "baseline"\
                                & Scheduler_mode == "{1}"'.format(self.scenario, self.scheduler_mode))
        if (baseline_row.empty):
            raise Exception("Baseline does not exist in pkl file")
        baseline_time = float(baseline_row['Total_time'])
        baseline_energy = float(baseline_row['Total_energy'])
        baseline_edp = baseline_energy * baseline_time
        t_min = baseline_time/5.0
        t_max = baseline_time*2
        t_step = baseline_time/100.0
        t_axis = np.arange(t_min, t_max, t_step)
        fig = go.Figure()
        baseline_edp_curve = go.Scatter(name="Baseline EDP-ISO curve", x=t_axis, y=baseline_edp/t_axis, fill='tozeroy', line = dict(color=px.colors.sequential.Greens[4]))
        fig.add_trace(baseline_edp_curve)
        # add constant curve to bg_color
        superior_constant = baseline_edp/t_min
        fig.add_scatter(x=[np.min(t_axis),np.max(t_axis)], y=[superior_constant, superior_constant], fill='tonexty', showlegend=False, mode='none')
        # add markers for every existing scenario
        df_scenario_and_perf = df.query('Scenario == "{0}" & Scheduler_mode == "{1}"'.format(self.scenario, self.scheduler_mode)).reset_index(drop=True)
        for index, row in df_scenario_and_perf.iterrows():
            fig.add_trace(go.Scatter(
            x=[row['Total_time']],
            y=[row['Total_energy']],
            marker=dict(color=px.colors.qualitative.Bold[index], size=12),
            mode="markers",
            name=UtilName.get_configuration_label(row['Configuration'])
            ))    
        fig.layout.xaxis.title.update(text="Time [s]")
        fig.layout.yaxis.title.update(text="Energy [J]")
        fig.layout.font = dict(family='Times New Roman', color='black')
        fig.layout.width = 500
        fig.layout.height = 500
        #fig.layout.yaxis.update(text="Energy (J)")
        #fig.show()
        #fig.write_html("{0}{1}-{2}.html".format(BASE_OUTPUT_PATH, self.scenario, self.scheduler_mode))  
        fig.write_image("{0}EDP-{1}-{2}.pdf".format(BASE_OUTPUT_PATH, self.scenario, self.scheduler_mode))
    
    def plot_edp_grouped(self):
        df = self.read_stats_dataframe()
        #old order alphabetical. used for date 19
        # scenarios = list(set(df.Scenario.values))
        # scenarios.sort()
        #new my order dissertation
        scenarios = ['smartphone_app','multitask','edge_computing','road_sign_detection','video_streaming','fp_driven']
        symbols = ["cross", "circle", "square", "diamond", "triangle-up", "star"]
        fig = make_subplots(rows=1, 
                            cols=len(scenarios),
                            vertical_spacing=0.025,
                            horizontal_spacing=0.045,
                            x_title='Time [ms]',
                            y_title='Energy [mJ]',
                            subplot_titles=(UtilName.get_scenarios_label(scenarios)))
        for idx_scenario, scenario in enumerate(scenarios):
            self.scenario = scenario
            show_legend = (idx_scenario==0) #used to generate legend only for the first scenario (avoid duplicating legend)
            #plot baseline edp-iso curve
            baseline_row = df.query('Scenario == "{0}"\
                                    & Configuration == "baseline"\
                                    & Scheduler_mode == "{1}"'.format(self.scenario, self.scheduler_mode))
            if (baseline_row.empty):
                raise Exception("Baseline does not exist in pkl file")
            baseline_time = float(baseline_row['Total_time'])*1000
            baseline_energy = float(baseline_row['Total_energy'])*1000
            baseline_edp = baseline_energy * baseline_time
            t_min = baseline_time/8.0
            t_max = baseline_time*2
            t_step = baseline_time/100.0
            t_axis = np.arange(t_min, t_max, t_step)
            baseline_edp_curve = go.Scatter(name="Baseline EDP-ISO curve", x=t_axis, y=baseline_edp/t_axis, fill='tozeroy', line = dict(color=px.colors.sequential.Greens[4]), showlegend=show_legend)
            fig.add_trace(baseline_edp_curve, row=1, col=(idx_scenario+1))
            # add constant curve to bg_color
            superior_constant = baseline_edp/t_min
            fig.add_scatter(x=[np.min(t_min),np.max(t_axis)], y=[superior_constant, superior_constant], fill='tonexty', showlegend=False, line = dict(color=px.colors.sequential.Reds[2]), row=1, col=(idx_scenario+1))
            # add markers for every existing scenario
            df_scenario_and_perf = df.query('Scenario == "{0}" & Scheduler_mode == "{1}"'.format(self.scenario, self.scheduler_mode)).reset_index(drop=True)
            for index, row in df_scenario_and_perf.iterrows():
                fig.add_trace(go.Scatter(
                x=[x * 1000 for x in [row['Total_time']]],
                y=[x * 1000 for x in [row['Total_energy']]],
                marker=dict(color=px.colors.qualitative.Set3[index], size=20, symbol=symbols[index], line=dict(color='black', width=2)),
                mode="markers",
                name=UtilName.get_configuration_label(row['Configuration']),
                showlegend=show_legend
                ), row=1, col=(idx_scenario+1))
            curr_y_axis = "yaxis{0}".format(idx_scenario+1)
            fig['layout'][curr_y_axis].update(range=[0, (superior_constant/4)], autorange=False)    

            fig.layout.font = dict(family='Times New Roman', color='black', size=28)
            fig.update_layout(
                autosize=False,
                width = 1600,
                height = 500,
                margin=go.layout.Margin(
                    l=70,
                    r=0,
                    b=0,
                    t=60
                )
            )
        for i in fig['layout']['annotations']:
            i['font'] = dict(size=28)
            if (i['text'] == 'Time [ms]'): #capture the annotation of the master x axis
                i['y'] = -0.1            # and correct its position.. unfortunately plotly does seem to have a neat way to do this
        fig.update_layout(legend=dict(y=-0.5, traceorder="normal"), legend_orientation="h")
        fig.write_image("{0}EDP-grouped-{1}.pdf".format(BASE_OUTPUT_PATH, self.scheduler_mode))


    def plot_bar_perf(self, pkl_file=STATS_FILE):
        #colors=["silver", "lightskyblue", "coral" ,"lightseagreen"]
        colors=["#6C8893", "#13A4B4", "#E8743B", "#19A979", "#2F6497", "#ED4A7B"]
        df = self.read_stats_dataframe()
        #old alphabetical order
        # scenarios = list(set(df.Scenario.values))
        # scenarios.sort()
        # configurations = list(set(df.Configuration.values))
        # configurations.sort()
        #new my order
        scenarios = ['smartphone_app','multitask','edge_computing','road_sign_detection','video_streaming','fp_driven']
        configurations = ['baseline', 'task_parallel', 'aes_accelerated', 'nn_accelerated', 'video_accelerated', 'accelerator_rich']
        fig = go.Figure()
        for idx_config, config in enumerate(configurations):
            config_df = df.query('Configuration == "{0}" & Scheduler_mode == "{1}"'.format(config, self.scheduler_mode))
            baseline_df = df.query('Configuration == "baseline" & Scheduler_mode == "{0}"'.format(self.scheduler_mode))
            speedup = []
            for i, val in enumerate(config_df.Total_time.values):
                speedup.append(baseline_df.Total_time.values[i] / config_df.Total_time.values[i])
            geomean_speedup = gmean(speedup)
            speedup.append(geomean_speedup)
            scenarios.append("geomean")
            fig.add_trace(go.Bar(
                x=UtilName.get_scenarios_label(scenarios),
                y=speedup,
                name=UtilName.get_configuration_label(config),
                marker=dict(color=colors[idx_config], line=dict(color='black', width=2) ),
                text=['{0:.2f}x'.format(x) for x in speedup],
                textposition="inside",
                textfont=dict(family='Times New Roman', size=24, color="black"),
                textangle=-90
                )
            )

        fig.update_layout(barmode='group', font=dict(family='Times New Roman', color='black', size=24))
        fig.layout.yaxis.title.update(text="Normalized Performance Speedup [x]")
        fig.layout.yaxis.update(tickformat='.2f',range=[0,3.3])
        fig.update_layout(legend_orientation="h")
        fig.update_yaxes(nticks=16, showgrid=True)
        fig.update_layout(
            autosize=False,
            width = 1600,
            height = 500,
            margin=go.layout.Margin(
                l=100,
                r=0,
                b=0,
                t=20
            ),
            #legend=dict(y=-0.2)
        )
        fig.show()
        fig.write_image("{0}{1}-policy-performance_bar.pdf".format(BASE_OUTPUT_PATH, self.scheduler_mode))

    def plot_bar_energy(self, pkl_file=STATS_FILE):
        #colors=["silver", "lightskyblue", "coral" ,"lightseagreen"]
        colors=["#6C8893", "#13A4B4", "#E8743B", "#19A979", "#2F6497", "#ED4A7B"]
        df = self.read_stats_dataframe()
        #old alphabetical order
        # scenarios = list(set(df.Scenario.values))
        # scenarios.sort()
        
        # configurations = list(set(df.Configuration.values))
        # configurations.sort()
        #new my order
        scenarios = ['smartphone_app','multitask','edge_computing','road_sign_detection','video_streaming','fp_driven']
        configurations = ['baseline', 'task_parallel', 'aes_accelerated', 'nn_accelerated', 'video_accelerated', 'accelerator_rich']
        fig = go.Figure()
        for idx_config, config in enumerate(configurations):
            config_df = df.query('Configuration == "{0}" & Scheduler_mode == "{1}"'.format(config, self.scheduler_mode))
            baseline_df = df.query('Configuration == "baseline" & Scheduler_mode == "{0}"'.format(self.scheduler_mode))
            saved_energy = []
            for i, val in enumerate(config_df.Total_energy.values):
                saved_energy.append(config_df.Total_energy.values[i] / baseline_df.Total_energy.values[i])
            geomean_saved_energy = gmean(saved_energy)
            saved_energy.append(geomean_saved_energy)
            scenarios.append("geomean")
            fig.add_trace(go.Bar(
                x=UtilName.get_scenarios_label(scenarios),
                y=saved_energy,
                name=UtilName.get_configuration_label(config),
                marker=dict(color=colors[idx_config], line=dict(color='black', width=2) ),
                text=['{0}%'.format(int(x*100)) for x in saved_energy],
                textposition="inside",
                textfont=dict(family='Times New Roman', size=24, color="black"),
                textangle=-90,
            ))

        fig.update_layout(barmode='group', font=dict(family='Times New Roman', color='black', size=24))
        fig.layout.yaxis.title.update(text="Normalized Energy Consumption [%]")
        fig.layout.yaxis.update(tickformat='.0%', range=[0,1.3])
        fig.update_layout(legend_orientation="h")
        fig.update_yaxes(nticks=20, showgrid=True)
        fig.update_layout(
            autosize=False,
            width = 1600,
            height = 500,
            margin=go.layout.Margin(
                l=100,
                r=0,
                b=0,
                t=20,
            ),
            #legend=dict(y=-0.2)
        )
        fig.show()
        fig.write_image("{0}{1}-policy-energy_bar.pdf".format(BASE_OUTPUT_PATH, self.scheduler_mode)) 

if __name__ == "__main__":
    # sys.stderr.write("Waiting for debugger attach")
    # sys.stderr.flush()
    # ptvsd.enable_attach(address=('localhost', 5678), redirect_output=True)
    # ptvsd.wait_for_attach()
    # breakpoint()
    stats = Stats()
    stats.scheduler_mode="performance"
    stats.plot_edp_grouped()
    #stats.plot_bar_perf()
    #stats.plot_bar_energy()
