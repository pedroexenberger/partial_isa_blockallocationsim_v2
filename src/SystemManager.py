import os
from Scheduler import Scheduler, SchedulerMode
from Core import Core
from Workload import Workload
from Stats import Stats

class SystemManager(object):
    def __init__(self, configuration, scenario, cores_description, workloads_description, scheduler_mode=SchedulerMode.Energy):
        self.time_base_ticks = 0
        self.scheduler = Scheduler(scheduler_mode, cores_description, workloads_description)
        self.configuration = configuration
        self.scenario = scenario
        self.stats = Stats()
        self.stats.configuration = configuration
        self.stats.scenario = scenario
        self.stats.scheduler_mode = scheduler_mode

    def run(self):
        while(self.scheduler.keep_execution()):
            target_core = self.scheduler.pick_core(at_least_ticks=self.time_base_ticks)
            #talvez aqui, se core nao tem workload escolhe e seta, e executa. se ja tem só executa.
            if (target_core.has_workload()):
                target_workload = target_core.workload
                target_core.process_workload()
            else:
                target_workload = self.scheduler.pick_workload_for_core(target_core)
                if (target_workload.pid is None):
                    self.sync_idle_core(target_core)
                else:
                    target_core.set_workload(target_workload)
                    target_core.process_workload()
            self.advance_time_base()
            self.scheduler.evaluate_migration(target_core, target_workload)

    def sync_idle_core(self, core):
        core_to_catch = self.scheduler.pick_core(at_least_ticks=core.cur_tick+1)
        if core_to_catch is not None:
            tick_to_catch = core_to_catch.cur_tick
            idle_ticks = tick_to_catch - core.cur_tick
            core.idle_ticks += idle_ticks
            core.cur_tick = tick_to_catch
        else:
            # avoid infinite loops when two cores are draw in time, but scheduler mode
            # always pick the same to execute, even though it cannot handle the current workload
            core.idle_ticks += 1
            core.cur_tick += 1

    def advance_time_base(self):
        self.time_base_ticks = self.scheduler.pick_core(at_least_ticks=self.time_base_ticks).cur_tick     

    def print_final_stats(self):
        self.stats.print_stats(self.scheduler.cores)
    
    def save_final_stats(self):
        self.stats.save_stats_dataframe(self.scheduler.cores)
    
    def plot_executions(self):
        self.stats.plot_executions()

    def plot_edp(self):
        self.stats.plot_edp()
