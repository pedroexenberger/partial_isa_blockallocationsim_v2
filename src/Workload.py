#!/usr/bin/python

from Block import Block
from Block import BlockType
from Block import ReadBlockStatus

BASE_BLOCK_FILE_PATH = "../inputs/"


class Workload(object):

    def __init__(self, workload_desc=None, pid=None, priority=None):

        self.pid = pid
        self.cur_tick = 0
        # self.is_critical = workload_desc["critical"]
        # self.is_integer = workload_desc["integer"]
        self.big_block = Block()
        self.little_block = Block()
        self.depends_on_isa_extension = None
        self.waiting_for_acc = False
        self.priority = priority
        self.prefer_core_support = None
        self.previous_holding_core_id = None
        if workload_desc:
            self.file_big_core = open("%s%s" % (BASE_BLOCK_FILE_PATH, workload_desc["file_big"]), "r")
            self.file_little_core = open("%s%s" % (BASE_BLOCK_FILE_PATH, workload_desc["file_little"]), "r")
            self.benchmark = workload_desc["benchmark"]

    def curr_block_type(self):
        assert (self.big_block.block_type == self.little_block.block_type), \
            "Non synchronized blocks. Found \
                Big BlockType <%s> (id:%d), while Little BlockType <%s> (id:%d)" % \
                    (self.big_block.block_type, self.big_block.block_id,
                    self.little_block.block_type, self.little_block.block_id)

        return self.big_block.block_type

    def read_block(self):
        self.waiting_for_acc = False

        read_status = self.big_block.read_block_from(self.file_big_core)
        read_status = self.little_block.read_block_from(self.file_little_core)

        if (read_status == ReadBlockStatus.OK):
            return read_status
        elif (read_status == ReadBlockStatus.FINISH):
            return read_status
        elif (read_status == ReadBlockStatus.NO_END):
            raise NotImplementedError
        elif (read_status == ReadBlockStatus.INVALID_START):
            raise NotImplementedError
        elif (read_status == ReadBlockStatus.READ_ACC):
            # Found accelerated block. Wait for the scheduler to decide how
            # to continue. If there is an acc, will call
            # read_accelerated_block_end. If there is no acc,
            # will call read_block again.
            self.waiting_for_acc = True
            return read_status
        elif (read_status == ReadBlockStatus.READ_END_ACC):
            # Ignore end of accelerated block and continue
            # (can only read end of acc in read_accelerated_block_end)
            return self.read_block()
        else:
            raise ValueError("Unknown ReadBlockStatus <%d>" % read_status)

    def read_accelerated_block_end(self):
        read_status = self.big_block.find_current_block_end(self.file_big_core)
        read_status = self.little_block.find_current_block_end(self.file_little_core)

        if (read_status == ReadBlockStatus.OK):
            self.waiting_for_acc = False
            return read_status
        elif (read_status == ReadBlockStatus.FINISH):
            raise ValueError("Accelerated block end not found. ReadBlockStatus <%d>" % read_status)
        elif (read_status == ReadBlockStatus.NO_END):
            raise ValueError("Accelerated block end not found. ReadBlockStatus <%d>" % read_status)
        elif (read_status == ReadBlockStatus.INVALID_START):
            raise ValueError("Accelerated block end not found. ReadBlockStatus <%d>" % read_status)
        elif (read_status == ReadBlockStatus.READ_ACC):
            raise ValueError("Accelerated block end not found. ReadBlockStatus <%d>" % read_status)
        else:
            raise ValueError("Unknown ReadBlockStatus <%d>" % read_status)

        self.waiting_for_acc = False

    def set_isa_extension_depency(self):
        "The current block depends on a specific core_type"
        self.depends_on_isa_extension = self.curr_block_type()

    def remove_isa_extension_dependency(self):
        "The current block can be allocated in any core"
        self.depends_on_isa_extension = None
        self.prefer_core_support = None

    def rewind_workload_to_previous_block(self):
        self.big_block.rewind_block(self.file_big_core)
        self.little_block.rewind_block(self.file_little_core)
