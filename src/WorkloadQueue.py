from Workload import Workload
import operator
from Stats import Stats

class WorkloadQueue(object):
    def __init__(self, workloads_description):
        self.queue = self.create_workloads_list(workloads_description)
        self.stats = Stats()

    def create_workloads_list(self, workloads_description):
        workloads = []
        for pid, workload_desc in enumerate(workloads_description):
            workload = Workload(workload_desc=workload_desc["workload"], pid=pid, priority=0)
            workloads.append(workload)
        return workloads

    def reorder_workloads_by_priority(self):
        self.queue.sort(key=operator.attrgetter('priority'))

    def is_empty(self):
        if len(self.queue) == 0:
            return True
        else:
            return False

    def remove_workload_from_queue(self, workload):
        self.queue.remove(workload)

    def append_workload_in_queue(self, workload):
        self.queue.append(workload)
    
    def insert_workload_in_queue_head(self, workload):
        self.queue.insert(0, workload)