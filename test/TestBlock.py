import sys
sys.path.append('../src')

#import ptvsd
import unittest

from Block import Block, BlockType, ReadBlockStatus

class TestBlock(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # vs code debug block 
        #5678 is the default attach port in the VS Code debug configurations
        # sys.stderr.write("Waiting for debugger attach")
        # ptvsd.enable_attach(address=('localhost', 5678), redirect_output=True)
        # ptvsd.wait_for_attach()
        # breakpoint()
        #end vs code debug block
        pass

    def setUp(self):
        self.file_little = open("little_file_simple.out", "r")
        self.file_acc = open("file_simple_block_with_blocks.out", "r")

    def tearDown(self):
        self.file_little.close()
        self.file_acc.close()

    def test_init_block(self):
        block = Block()
        self.assertEqual(block.block_type, BlockType.NONE)

    def test_read_block(self):
        little_block = Block()
        little_block.read_block_from(self.file_little)
        self.assertEqual(little_block.block_start, 22334428)
        self.assertEqual(little_block.block_end, 22392646)
        self.assertEqual(little_block.block_type, BlockType.FP)
        self.assertEqual(little_block.block_id, 1)

    def test_find_block_end(self):
        little_block = Block()
        block_status = little_block.read_block_from(self.file_acc)
        self.assertEqual(little_block.block_start, 22334428)
        self.assertEqual(little_block.block_type, BlockType.VIDEO)
        self.assertEqual(little_block.block_id, 1)
        self.assertEqual(little_block.block_end, 0)
        self.assertEqual(block_status, ReadBlockStatus.READ_ACC)
        little_block.find_current_block_end(self.file_acc)
        self.assertEqual(little_block.block_end, 92002594)

    def test_rewind_block(self):
        little_block = Block()
        little_block.read_block_from(self.file_little)
        self.assertEqual(little_block.block_start, 22334428)
        self.assertEqual(little_block.block_end, 22392646)
        self.assertEqual(little_block.block_type, BlockType.FP)
        self.assertEqual(little_block.block_id, 1)
        little_block.rewind_block(self.file_little)
        self.assertEqual(little_block.block_start, 22334428)
        self.assertEqual(little_block.block_end, 22392646)
        self.assertEqual(little_block.block_type, BlockType.FP)
        self.assertEqual(little_block.block_id, 1)
        little_block.read_block_from(self.file_little)
        self.assertEqual(little_block.block_start, 22334428)
        self.assertEqual(little_block.block_end, 22392646)
        self.assertEqual(little_block.block_type, BlockType.FP)
        self.assertEqual(little_block.block_id, 1)
        