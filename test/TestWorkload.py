import sys
sys.path.append('../src')

import json

#import ptvsd
import unittest

from Workload import Workload
from Block import Block, BlockType, ReadBlockStatus

class TestWorkload(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # vs code debug block 
        #5678 is the default attach port in the VS Code debug configurations
        #sys.stderr.write("Waiting for debugger attach")
        #ptvsd.enable_attach(address=('localhost', 5678), redirect_output=True)
        #ptvsd.wait_for_attach()
        # breakpoint()
        #end vs code debug block
        pass

    def setUp(self):
        with open("workloads_simple.json", "r") as f:
            self.simple_workloads = json.load(f)["workloads"]
        
        with open("workloads_acc.json", "r") as f:
            self.acc_workloads = json.load(f)["workloads"]

    def test_init_workload(self):
        bench = self.simple_workloads[0]
        workload = Workload(bench["workload"], 1, 5)
        self.assertEqual(workload.pid, 1)
        self.assertEqual(workload.cur_tick, 0)
        self.assertNotEqual(workload.file_big_core, None)
        self.assertNotEqual(workload.file_little_core, None)
        self.assertEqual(workload.benchmark, "test_workload")
        #self.assertEqual(workload.is_integer, False)
        self.assertEqual(workload.priority, 5)

    
    def test_read_block(self):
        bench = self.simple_workloads[0]
        workload = Workload(bench["workload"], 1, 5)
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.OK)
        self.assertEqual(workload.big_block.block_start, 22334428)
        self.assertEqual(workload.little_block.block_start, 22334428)
        self.assertEqual(workload.big_block.block_end, 22392646)
        self.assertEqual(workload.little_block.block_end, 22392646)
        
        bench = self.acc_workloads[0]
        workload = Workload(bench["workload"], 1, 5)
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.READ_ACC)
        self.assertEqual(workload.big_block.block_start, 22334428)
        self.assertEqual(workload.big_block.block_end, 0)
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.OK)
        self.assertEqual(workload.big_block.block_start, 22334500)
        self.assertEqual(workload.big_block.block_end, 22392646)
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.FINISH)
        #Still need to test the other cases

    
    def test_curr_block_type(self):
        bench = self.simple_workloads[0]
        workload = Workload(bench["workload"], 1, 5)
        workload.read_block()
        
        self.assertEqual(workload.curr_block_type(), BlockType.FP)
        
        workload.big_block.block_type = BlockType.INT
        with self.assertRaises(AssertionError):
            workload.curr_block_type()


    
    def test_read_accelerated_block_end(self):
        bench = self.acc_workloads[0]
        workload = Workload(bench["workload"], 1, 5)
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.READ_ACC)
        self.assertEqual(workload.big_block.block_start, 22334428)
        self.assertEqual(workload.big_block.block_end, 0)
        read_status = workload.read_accelerated_block_end()
        self.assertEqual(read_status, ReadBlockStatus.OK)
        self.assertEqual(workload.big_block.block_start, 22334428)
        self.assertEqual(workload.big_block.block_end, 92002594)

    def test_rewind_workload_to_previous_block(self):
        bench = self.simple_workloads[0]
        workload = Workload(bench["workload"], 1, 5)
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.OK)
        self.assertEqual(workload.big_block.block_start, 22334428)
        self.assertEqual(workload.little_block.block_start, 22334428)
        self.assertEqual(workload.big_block.block_end, 22392646)
        self.assertEqual(workload.little_block.block_end, 22392646)
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.OK)
        self.assertEqual(workload.big_block.block_start, 92002594)
        self.assertEqual(workload.little_block.block_start, 92002594)
        self.assertEqual(workload.big_block.block_end, 92002594)
        self.assertEqual(workload.little_block.block_end, 92002594)
        workload.rewind_workload_to_previous_block()
        read_status = workload.read_block()
        self.assertEqual(read_status, ReadBlockStatus.OK)
        self.assertEqual(workload.big_block.block_start, 22334428)
        self.assertEqual(workload.little_block.block_start, 22334428)
        self.assertEqual(workload.big_block.block_end, 22392646)
        self.assertEqual(workload.little_block.block_end, 22392646)
        

